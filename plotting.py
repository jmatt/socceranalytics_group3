import matplotlib.pyplot as plt
import matplotlib as mpl
import seaborn as sns
import mplsoccer as mpls
import numpy as np
import pandas as pd
import os
import shutil
import moviepy.video.io.ImageSequenceClip as moviepy
from tqdm.auto import tqdm
from scipy.spatial import ConvexHull


# ----- plotting params ----- #
plt.rcParams.update({'font.family': 'sans-serif'})
plt.rcParams.update({'font.sans-serif': 'yu gothic'})
plt.rcParams.update({'font.size': 20})
plt.rcParams.update({'figure.autolayout': True})

figsize = {'horiz': (17, 13), 'vert': (12, 18)}
title_fontsize = 35
suptitle_fontsize = 25

team_colors = {
    'Denmark': '#db2c2c',
    'Finland': '#2e68c7',
    0: '#db2c2c',
    1: '#2e68c7'
}
team_cmaps = {
    'Denmark': 'afmhot',
    'Finland': 'viridis',
    0: 'afmhot',
    1: 'viridis'
}
event_colors = {
    'pass': 'white', #was cornflowerblue
    'duel': 'red',
    'game_interruption': 'black', #was palegreen
    'throw_in': 'white', #was navy
    'interception': 'aqua', #was darkred
    'touch': 'lime',
    'infraction': 'hotpink',
    'free_kick': 'blue', #was slateblue
    'corner': 'blue', #was dodgerblue
    'shot': 'orange', #was deepskyblue
    'shot_against': 'black', #was darkgreen
    'clearance': 'black', #was crimson
    'goal_kick': 'black', #was steelblue
    'goalkeeper_exit': 'black', #was darkseagreen
    'acceleration': 'black', #was aqua
    'fairplay': 'black', #was yellowgreen
    'offside': 'black', #was green
    'penalty': 'orange' #was blue
}
# --------------------------- #


def match_statistics(wyscout, tracab, source='data'):

    if source == 'UEFA':
        stats = {
            'Performance': {
                'Possession (%)': (64, 36),
                'Passing accuracy (%)': (84, 66),
                'Passes attempted': (655, 283),
                'Passes completed': (553, 186),
                'Distance covered (km)': (105.36, 101.74)
            },
            'Attacking': {
                'Goals': (0, 1),
                'Total attempts': (23, 1),
                'On target': (6, 1),
                'Off target': (10, 0),
                'Blocked': (7, 0),
                'Woodwork': (0, 0),
                'Corners taken': (9, 0),
                'Offsides': (0, 1)
            },
            'Defending': {
                'Balls recovered': (40, 30),
                'Tackles': (9, 18),
                'Blocks': (0, 7),
                'Clearances completed': (4, 22)
            },
            'Disciplinary': {
                'Yellow cards': (0, 2),
                'Red cards': (0, 0),
                'Fouls committed': (11, 12)
            }
        }
    elif source == 'data':
        stats = wyscout.calculate_match_stats()
        distances = tracab.distance_covered(ballInPlay=False, sampling_sec=2)
        stats['Performance']['Distance covered (km)'] = [float(f"{distances['Denmark (tot)']:.2f}"),
                                                         float(f"{distances['Finland (tot)']:.2f}")]

    fig, axs = plt.subplots(len(stats), 1, figsize=figsize['vert'], sharex=True,
                            gridspec_kw={'height_ratios': [len(stats[category]) for category in stats.keys()]})

    for i, category in enumerate(stats):
        ax = axs[i]
        ax.set_title(category, loc='left')
        for stat in list(stats[category].keys())[::-1]:
            den_val = stats[category][stat][0]
            fin_val = stats[category][stat][1]
            if sum((den_val, fin_val)) > 0:
                den_val_norm, fin_val_norm = den_val / sum((den_val, fin_val)), fin_val / sum((den_val, fin_val))
            else:
                den_val_norm, fin_val_norm = 0.5, 0.5
            ax.barh(stat, den_val_norm, 0.8, color=team_colors['Denmark'], alpha=0.8)
            ax.barh(stat, fin_val_norm, 0.8, color=team_colors['Finland'], alpha=0.8, left=den_val_norm)
            # ax.axvline(0.5, color='white', ls=':')
            ax.annotate(den_val, (0.02, stat), size=18, color='white', ha='left', va='center', weight='bold')
            ax.annotate(fin_val, (0.98, stat), size=18, color='white', ha='right', va='center', weight='bold')
            ax.annotate(stat, (0.5, stat), size=18, color='white', ha='center', va='center', weight='bold')

    ax.set_xlim(0, 1)
    for ax in axs:
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)
        for spine in ["bottom", "top", "left", "right"]:
            ax.spines[spine].set_visible(False)

    plt.suptitle("Match statistics from "+source, fontsize=title_fontsize)
    plt.savefig('graphs/match_statistics_'+source+'.png')
    plt.show()


def draw_pitch(data, orientation, half=False):

    if orientation in ['horiz', 'horizontal']:
        create_pitch = mpls.Pitch
        orientation = 'horiz'
    elif orientation in ['vert', 'vertical']:
        create_pitch = mpls.VerticalPitch
        orientation = 'vert'

    if data.provider == 'tracab':
        pitch_length = data.pitch_dims[0] / 100
        pitch_width = data.pitch_dims[1] / 100
    else:
        pitch_length = None
        pitch_width = None

    pitch = create_pitch(pitch_type=data.provider, pitch_color='#aabb97', line_color='white', stripe_color='#c2d59d',
                         stripe=True, pitch_length=pitch_length, pitch_width=pitch_width, linewidth=4, spot_scale=0.004,
                         half=half)

    figsize_pitch = figsize[orientation]
    if half:
        figsize_pitch = (figsize_pitch[0], figsize_pitch[1]/2+1)

    fig, ax = pitch.draw(figsize=figsize_pitch)
    return fig, ax


def shot_map(data, show_xg='size'):

    if show_xg not in ['size', 'alpha']:
        print("Unknown method to visualize XG values.")
        return

    shots = data.shots

    for team in data.teams:

        fig, ax = draw_pitch(data, 'vert', half=True)
        xg = {'statsbomb': 'shot.statsbomb_xg', 'wyscout': 'shot.xg'}

        for i, shot in shots.iterrows():

            if shot['team.name'] != team:
                continue

            if data.provider == 'wyscout':
                loc = (shot['location.x'], shot['location.y'])
                goal = shot['shot.isGoal']
            elif data.provider == 'statsbomb':
                loc = shot.location

            # loc = [data.pitch_dims[0] - loc[0], loc[1]]

            if show_xg == 'alpha':
                alpha = (0.7 + 0.3 * shot[xg[data.provider]]/np.max(shots[xg[data.provider]]))
                markersize = 3
            elif show_xg == 'size':
                alpha = 0.7
                markersize = 70**shot[xg[data.provider]] * 10

            marker = 'o'
            if goal:
                # marker = '*'
                markeredgecolor = '#33b500'     # green
                markeredgewidth = 3
            else:
                # marker = 'o'
                markeredgecolor = None
                markeredgewidth = 0

            ax.plot(loc[1], loc[0], marker=marker, color=team_colors[team], markersize=markersize,
                    alpha=alpha, ls='', markeredgecolor=markeredgecolor, markeredgewidth=markeredgewidth)

            # ax.plot(loc[0], loc[1], marker='x', color=color, alpha=1, ls='')

        ax.set_title("Shotmap "+team, pad=20, fontsize=title_fontsize)
        plt.savefig('graphs/shotmap_'+team+'.png')
        plt.show()


def starting_xi(statsbomb):

    fig, ax = draw_pitch(statsbomb, 'vert')

    formations, line_ups = statsbomb.formations_lineups()
    positions = {
        433: {
            'G': [0.06, 0.5],
            'LB': [0.2, 0.2],
            'RB': [0.2, 0.8],
            'LCB': [0.18, 0.4],
            'RCB': [0.18, 0.6],
            'LCM': [0.325, 0.25],
            'CDM': [0.305, 0.5],
            'RCM': [0.325, 0.75],
            'LW': [0.43, 0.25],
            'CF': [0.43, 0.5],
            'RW': [0.43, 0.75]
        },
        352: {
            'G': [0.06, 0.5],
            'LCB': [0.18, 0.25],
            'CB': [0.18, 0.5],
            'RCB': [0.18, 0.75],
            'LWB': [0.35, 0.12],
            'RWB': [0.35, 0.88],
            'LCM': [0.3, 0.3],
            'CDM': [0.27, 0.5],
            'RCM': [0.3, 0.7],
            'LCF': [0.43, 0.4],
            'RCF': [0.43, 0.6]
        }
    }

    change_names = {
        "Martin Braithwaite Christensen": "Braithwaite",
        'Daniel O"Shaughnessy': "O'Shaughnessy"
    }

    for team in [0, 1]:
        formation = formations[team]
        color = team_colors[team]
        for i, player in line_ups[team].iterrows():

            if player['Name'] in change_names:
                name = change_names[player['Name']]
            else:
                name = player['Name'].split()[-1]

            position = ''.join([pos[0] for pos in player['Position'].split()])
            loc = positions[formation][position]
            if team == 0:
                loc = (loc[1]*statsbomb.pitch_dims[1],
                       loc[0]*statsbomb.pitch_dims[0])
            else:
                loc = (statsbomb.pitch_dims[1]-loc[1]*statsbomb.pitch_dims[1],
                       statsbomb.pitch_dims[0]-loc[0]*statsbomb.pitch_dims[0])

            ax.plot(loc[0], loc[1], marker='o', color=color,
                    markersize=60, alpha=0.8, ls='', markeredgecolor=color, markeredgewidth=1)
            ax.annotate(player['Jersey Number'], loc, size=35, color='white', ha='center', va='center', weight='bold')
            ax.annotate(name, [loc[0], loc[1]-5], size=25, color='white', ha='center', va='center',
                        weight='bold')

    ax.set_title("Starting XIs", pad=20, fontsize=title_fontsize)
    plt.savefig('graphs/starting_xi.png')
    plt.show()


def real_starting_xi(tracab, shift_teams=False):

    # for team in [0, 1]:

    fig, ax = draw_pitch(tracab, 'vert')

    avg_pos = tracab.average_positions()

    for id in tracab.obj_info:
        obj = tracab.obj_info[id]
        team = int(obj['type'])
        if team == 7 or pd.isna(tracab.XY.iloc[0, :][id+'.x']):
            # obj is ball or player is not in starting XI
            continue
        else:
            loc = avg_pos[id]
            color = team_colors[team]
            if shift_teams:
                if team == 0:
                    if obj['name'] == "Kasper Schmeichel":
                        loc[0] -= 1500
                    else:
                        loc[0] -= 2600
                elif team == 1:
                    loc[0] += 800
            # if team == 1:
            #     loc[0], loc[1] = -loc[0], -loc[1]
            ax.plot(loc[1], loc[0], marker='o', color=color,
                    markersize=60, alpha=0.8, ls='', markeredgecolor=color, markeredgewidth=1)
            ax.annotate(obj['jersey number'], (loc[1], loc[0]), size=35, color='white', ha='center', va='center', weight='bold')
            # ax.annotate(obj['name'].split()[-1], [loc[0], loc[1]-50], size=25, color='white', ha='center', va='center',
            #             weight='bold')
    ax.set_title("Real-tactical starting XIs", pad=20, fontsize=title_fontsize)
    plt.savefig('graphs/real_starting_xi.png')
    plt.show()


def freeze_frame(tracab, name, time, convex_hulls=[], show=True):

    frame = tracab.XY.loc[time, :]

    fig, ax = draw_pitch(tracab, 'vert')

    teams = {
        '0': 'Denmark',
        '1': 'Finland'
    }
    all_player_locs = {
        'Denmark': [],
        'Finland': []
    }

    for id in tracab.obj_info:
        loc = (frame[id+'.x'], frame[id+'.y'])
        if pd.isna(loc[0]) or pd.isna(loc[1]):
            continue
        obj = tracab.obj_info[id]
        if obj['type'] == '7':
            # ball
            color = 'white'
            markersize = 20
            txt = ''
        else:
            team = teams[obj['type']]
            if obj['name'] not in ['Kasper Schmeichel', 'Lukáš Hrádecký']:
                all_player_locs[team].append(loc)
            # player; type 0: home team, type 1: away team
            color = team_colors[team]
            markersize = 50
            txt = obj['jersey number']

        ax.plot(loc[1], loc[0], marker='o', color=color,
                markersize=markersize, alpha=0.8, ls='', markeredgecolor=color, markeredgewidth=1)
        ax.annotate(txt, (loc[1], loc[0]), size=25, color='white', ha='center', va='center', weight='bold')

    for team in convex_hulls:

        players = np.array(all_player_locs[team])
        # print(players)
        hull = ConvexHull(players)
        ax.plot(np.append(players[hull.vertices, 1], players[hull.vertices[0], 1]),
                np.append(players[hull.vertices, 0], players[hull.vertices[0], 0]),
                color=team_colors[team], lw=5)
        ax.fill(players[hull.vertices, 1], players[hull.vertices, 0], color=team_colors[team], alpha=0.3)

    if show:
        plt.savefig('graphs/freeze_frames/'+name+'.png')
        plt.show()

    return fig


def animate_tracab(tracab, name, start, end, convex_hulls=[]):
    print("Start rendering of tracab scene:", name)
    # Series of all the timestamps to plot
    start, end = pd.to_datetime(start), pd.to_datetime(end)
    timestamps = list(tracab.XY.loc[start:end].index)

    # create folder for pngs
    path = tracab.match_folder + "video_buf/"
    try:
        os.mkdir(path)
    except OSError:
        print("Directory %s already exists." % path)
    else:
        print("Successfully created the directory %s." % path)

    # save freeze frame for each timestamp as png
    print("Create and save freeze frames...")
    for i, timestamp in enumerate(tqdm(timestamps)):
        fig = freeze_frame(tracab, name=None, time=timestamp, convex_hulls=convex_hulls, show=False)
        plt.figure(fig.number)
        img_name = "000000" + str(i)
        img_name = img_name[-6:]
        img_path = path + img_name + ".png"
        plt.savefig(img_path)
        plt.close()
    print("Combine freeze frames into video...")
    # combine all the created pngs into a video
    image_files = [path + img for img in os.listdir(path) if img.endswith(".png")]
    clip = moviepy.ImageSequenceClip(image_files, fps=25)
    clip.write_videofile('graphs/animations/'+name+'.mp4')

    shutil.rmtree(path, ignore_errors=False, onerror=None)


def player_heatmap(tracab, player_name, start=None, end=None, method='kde'):

    if method not in ['kde', 'hist']:
        print("Unknown method")
        return

    cmap = 'afmhot'
    alpha = 0.6

    player_id = [key for key in tracab.obj_info.keys() if tracab.obj_info[key]['name'] == player_name][0]
    xy = tracab.XY.loc[pd.to_datetime(start):pd.to_datetime(end)]
    xy = xy[xy['ballInPlay']]
    x_locs = xy[player_id+'.x'].resample('1S').mean()
    y_locs = xy[player_id+'.y'].resample('1S').mean()

    fig, ax = draw_pitch(tracab, 'vert')

    if method == 'kde':
        sns.kdeplot(x=y_locs, y=x_locs, ax=ax, clip=([-3400, 3400], [-5250, 5250]), bw_adjust=0.7, levels=10,
                    cmap=cmap, fill=True, alpha=alpha, cbar=False,
                    cbar_kws={'shrink': 0.89, 'location': 'bottom', 'pad': 0.01, 'fraction': 0.04, 'aspect': 30})
    else:
        sns.histplot(x=y_locs, y=x_locs, ax=ax, cmap=cmap, alpha=alpha, bins='stone')

    ax.set_title("Heatmap of "+player_name, pad=20, fontsize=title_fontsize)
    plt.savefig('graphs/player_heatmaps/'+player_name+'.png')
    plt.show()


def players_running(tracab, substitutes=False):

    fig, ax = plt.subplots(1, 1, figsize=figsize['horiz'])

    distances = tracab.distance_covered(sampling_sec=1, normalize90min=True)
    velocities = tracab.velocities_percentile(sampling_sec=1, percentile=99.92)

    for id in tracab.obj_info:
        name = tracab.obj_info[id]['name']
        alpha = 1
        if pd.isna(tracab.XY.iloc[0, :][id + '.x']):
            # player is not in starting XI
            if substitutes:
                alpha = 0.5
            else:
                continue
        if name in ['Ball', 'unknown', 'Lukáš Hrádecký', 'Kasper Schmeichel']:
            continue
        color = team_colors[int(tracab.obj_info[id]['type'])]
        jersey_number = tracab.obj_info[id]['jersey number']
        ax.scatter(distances[name], velocities[name], s=2000, alpha=alpha, color=color)
        ax.annotate(jersey_number, (distances[name], velocities[name]), size=25, color='white',
                    ha='center', va='center', weight='bold')

    ax.set_xlabel('Distance covered [km/90min]')
    ax.set_ylabel('Maximum pace [m/s]')
    ax.grid(alpha=0.5)
    ax.set_axisbelow(True)
    for spine in ["top", "right"]:
        ax.spines[spine].set_visible(False)

    ax.set_title("Running performance of players", pad=20, fontsize=title_fontsize)
    plt.savefig('graphs/players_running.png')
    plt.show()


def action_map(wyscout, player_name):

    fig, ax = draw_pitch(wyscout, 'vert')

    player_events = wyscout.get_events_by_player(player_name)

    for i, event in player_events.iterrows():
        ax.plot(event['location.y'], event['location.x'], marker='o', ls='', markersize=15, alpha=0.8,
                color=event_colors.get(event['type.primary'], 'black'))

    ax.set_title("Action map of "+player_name, pad=20, fontsize=title_fontsize)
    plt.savefig('graphs/action_map_'+player_name+'.png')
    plt.show()


def player_passes(wyscout, player_name):

    fig, ax = draw_pitch(wyscout, 'vert')

    passes = wyscout.passes.loc[wyscout.passes['player.name'] == player_name].reset_index(drop=True)
    passes_recieved = wyscout.passes.loc[wyscout.passes['pass.recipient.name'] == player_name].reset_index(drop=True)
    if passes.at[0, 'team.name'] == 'Denmark':
        color = team_colors[0]
    elif passes.at[0, 'team.name'] == 'Finland':
        color = team_colors[1]
    else:
        color = 'white'

    for i, event in passes.iterrows():
        start_x = event['location.x']
        start_y = event['location.y']
        if event['pass.recipient.id'] != 0:
            # draw arrow
            end_x = event['pass.endLocation.x']
            end_y = event['pass.endLocation.y']
            ax.annotate("", xy=(end_y, end_x), xytext=(start_y, start_x),
                        arrowprops=dict(arrowstyle='-|>,head_width=0.4,head_length=0.8',
                                        color=color,
                                        lw=5,
                                        ls='-',
                                        alpha=0.9),
                        zorder=3)
        else:
            ax.plot(start_y, start_x, marker='x', ls='', markersize=20, alpha=0.9, color=color)

    for i, event in passes_recieved.iterrows():
        start_x = event['location.x']
        start_y = event['location.y']
        if event['pass.recipient.id'] != 0:
            # draw arrow
            end_x = event['pass.endLocation.x']
            end_y = event['pass.endLocation.y']
            ax.annotate("", xy=(end_y, end_x), xytext=(start_y, start_x),
                        arrowprops=dict(arrowstyle='->,head_width=0.25,head_length=0.5',
                                        color='black',
                                        lw=5,
                                        ls='-',
                                        alpha=0.3),
                        zorder=2)
        else:
            pass

    ax.set_title("Passing map of "+player_name, pad=20, fontsize=title_fontsize)
    plt.savefig('graphs/player_passes_'+player_name+'.png')
    plt.show()


def game_timeline(wyscout, figsize=(12, 3), show=True):

    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=figsize, sharey=True)

    periods = wyscout.get_timestamps_periods()

    stoppage_time = {
        '1H': pd.to_timedelta(periods['end_1H']) - pd.Timedelta(45, 'min'),
        '2H': pd.to_timedelta(periods['end_2H']) - pd.Timedelta(90, 'min')
    }

    ax1.set_xlim(pd.to_datetime(periods['start_1H']), pd.to_datetime(periods['end_1H']))
    ax2.set_xlim(pd.to_datetime(periods['start_2H']), pd.to_datetime(periods['end_2H']))

    # plot vertical lines at whistles / stoppage times
    ax1.axvline(pd.to_datetime('00:45:00.000'), ymax=0.15, color='green', ls=':')
    ax2.axvline(pd.to_datetime('01:30:00.000'), ymax=0.15, color='green', ls=':')

    ax1.set_xticks(pd.date_range('00:00:00.000', '00:45:00.000', 10),
                   labels=[str(int(5*i))+"'" for i in range(10)])
    ax2.set_xticks(pd.date_range('00:45:00.000', '01:30:00.000', 10),
                   labels=[str(int(5*i))+"'" for i in range(9, 19)])

    # display stoppage times
    ax1.annotate("+"+str(stoppage_time['1H'].seconds//60)+"'", [pd.to_datetime('00:45:00.000')+stoppage_time['1H']/2, 0.07],
                 xycoords=('data', 'axes fraction'), ha='center', va='center', color='green', size=15)
    ax2.annotate("+"+str(stoppage_time['2H'].seconds//60)+"'", [pd.to_datetime('01:30:00.000')+stoppage_time['2H']/2, 0.07],
                 xycoords=('data', 'axes fraction'), ha='center', va='center', color='green', size=15)

    for ax in [ax1, ax2]:
        ax.get_yaxis().set_visible(False)
        for spine in ["left", "top", "right"]:
            ax.spines[spine].set_visible(False)

    if show:
        plt.show()
    return fig, (ax1, ax2)


def game_events(wyscout):

    fig, (ax1, ax2) = game_timeline(wyscout, figsize=(17, 4), show=False)

    goals = wyscout.shots[wyscout.shots['shot.isGoal']]
    yellow_cards = wyscout.fouls[wyscout.fouls['infraction.yellowCard']]
    red_cards = wyscout.fouls[wyscout.fouls['infraction.redCard']]
    penalties = wyscout.get_events_by_type('penalty')
    missed_penalties = penalties[~penalties['shot.isGoal']]
    # substitutions = wyscout.playerstats[(0 < wyscout.playerstats['total.minutesOnField']) &
    #                                     (99 > wyscout.playerstats['total.minutesOnField'])]
    ls = '--'

    y_levels = [0.25, 0.5, 0.75]

    for i, goal in goals.iterrows():

        if goal['matchPeriod'] == '1H':
            ax = ax1
        else:
            ax = ax2

        y_level = y_levels[2]   # random.choice(y_levels)
        ax.annotate(u"\u26bd", [pd.to_datetime(goal['matchTimestamp']), y_level],
                    xycoords=('data', 'axes fraction'), ha='center', va='center', color='black', size=20)
        ax.axvline(pd.to_datetime(goal['matchTimestamp']), ymax=y_level, color=team_colors[goal['team.name']], ls=ls)

    for i, yellow_card in yellow_cards.iterrows():
        if yellow_card['matchPeriod'] == '1H':
            ax = ax1
        else:
            ax = ax2
        y_level = y_levels[0]
        ax.annotate(u"\u25A0", [pd.to_datetime(yellow_card['matchTimestamp']), y_level], color='yellow',
                    xycoords=('data', 'axes fraction'), ha='center', va='center', size=20)
        ax.axvline(pd.to_datetime(yellow_card['matchTimestamp']), ymax=y_level,
                   color=team_colors[yellow_card['team.name']], ls=ls)

    for i, red_card in red_cards.iterrows():
        if red_card['matchPeriod'] == '1H':
            ax = ax1
        else:
            ax = ax2
        y_level = y_levels[1]
        ax.annotate(u"\u25A0", [pd.to_datetime(red_card['matchTimestamp']), y_level], color='red',
                    xycoords=('data', 'axes fraction'), ha='center', va='center', size=20)
        ax.axvline(pd.to_datetime(red_card['matchTimestamp']), ymax=y_level,
                   color=team_colors[red_card['team.name']], ls=ls)

    for i, missed_penalty in missed_penalties.iterrows():
        if missed_penalty['matchPeriod'] == '1H':
            ax = ax1
        else:
            ax = ax2
        y_level = y_levels[1]
        ax.annotate(u"\u2573", [pd.to_datetime(missed_penalty['matchTimestamp']), y_level], color='red',
                    xycoords=('data', 'axes fraction'), ha='center', va='center', size=30, weight='bold')
        ax.axvline(pd.to_datetime(missed_penalty['matchTimestamp']), ymax=y_level,
                   color=team_colors[missed_penalty['team.name']], ls=ls)

        substitutions = {
            'Denmark': [
                {'matchPeriod': '1H', 'matchTimestamp': "00:43:00.000"},
                {'matchPeriod': '2H', 'matchTimestamp': "01:03:00.000"},
                {'matchPeriod': '2H', 'matchTimestamp': "01:03:00.000"},
                {'matchPeriod': '2H', 'matchTimestamp': "01:16:00.000"},
                {'matchPeriod': '2H', 'matchTimestamp': "01:16:00.000"}
            ],
            'Finland': [
                {'matchPeriod': '2H', 'matchTimestamp': "01:16:00.000"},
                {'matchPeriod': '2H', 'matchTimestamp': "01:16:00.000"},
                {'matchPeriod': '2H', 'matchTimestamp': "01:24:00.000"},
                {'matchPeriod': '2H', 'matchTimestamp': "01:30:00.000"}
            ]
        }
        for team in wyscout.teams:
            for sub in substitutions[team]:
                if sub['matchPeriod'] == '1H':
                    ax = ax1
                else:
                    ax = ax2
                y_level = y_levels[0]
                ax.annotate(u"\u25B2", [pd.to_datetime(sub['matchTimestamp']), y_level+0.035], color='green',
                            xycoords=('data', 'axes fraction'), ha='center', va='center', size=10, weight='bold')
                ax.annotate(u"\u25BC", [pd.to_datetime(sub['matchTimestamp']), y_level-0.035], color='red',
                            xycoords=('data', 'axes fraction'), ha='center', va='center', size=10, weight='bold')
                ax.axvline(pd.to_datetime(sub['matchTimestamp']), ymax=y_level,
                           color=team_colors[team], ls=ls)

    plt.suptitle("Timeline of game events", fontsize=title_fontsize)
    plt.savefig('graphs/game_events_timeline.png')
    plt.show()


def xgoals_racechart(wyscout):

    shots = pd.concat((wyscout.shots, wyscout.get_events_by_type('penalty'))).sort_values(['matchPeriod', 'minute', 'second'])
    team_names = wyscout.teams

    periods = wyscout.get_timestamps_periods()

    fig, (ax1, ax2) = game_timeline(wyscout, figsize=(17, 7), show=False)

    for i, team in enumerate(team_names):
        xgoals_1half = np.cumsum(shots['shot.xg'][(shots['team.name'] == team) & (shots['matchPeriod'] == '1H')]).to_numpy()
        xgoals_2half = np.cumsum(shots['shot.xg'][(shots['team.name'] == team) & (shots['matchPeriod'] == '2H')]).to_numpy()
        timestamps_1half = shots['matchTimestamp'][(shots['team.name'] == team) & (shots['matchPeriod'] == '1H')].to_numpy()
        timestamps_2half = shots['matchTimestamp'][(shots['team.name'] == team) & (shots['matchPeriod'] == '2H')].to_numpy()

        # handle no shots in a half
        if len(xgoals_1half) == 0:
            xgoals_1half = np.array([0.0])
            timestamps_1half = np.array(['00:25:00.000'])
        if len(xgoals_2half) == 0:
            xgoals_2half = np.array([0.0])
            timestamps_2half = np.array(['00:25:00.000'])

        # add 1st half end value to values of 2nd half
        xgoals_2half = xgoals_2half + xgoals_1half[-1]

        # pad 0th and 90th minute:
        xgoals_1half = np.concatenate(([0.0], xgoals_1half, [xgoals_1half[-1]]))
        xgoals_2half = np.concatenate(([xgoals_1half[-1]], xgoals_2half, [xgoals_2half[-1]]))
        timestamps_1half = pd.to_datetime(np.concatenate(([periods['start_1H']], timestamps_1half, [periods['end_1H']])))
        timestamps_2half = pd.to_datetime(np.concatenate(([periods['start_2H']], timestamps_2half, [periods['end_2H']])))

        ax1.step(pd.to_datetime(timestamps_1half), xgoals_1half, color=team_colors[i], where='post', lw=3)
        ax2.step(pd.to_datetime(timestamps_2half), xgoals_2half, color=team_colors[i], where='post', lw=3)

    ax1.set_ylim(bottom=-0.02)
    ax1.get_yaxis().set_visible(True)
    ax1.spines['left'].set_visible(True)

    ax1.set_ylabel('Expected goals (xG)')

    plt.suptitle("xGoals racechart", fontsize=title_fontsize)
    plt.savefig('graphs/xgoals_racechart.png')
    plt.show()


def game_momentum(tracab, wyscout):

    fig, (ax1, ax2) = game_timeline(wyscout, figsize=(17, 5), show=False)

    xy = tracab.XY_with_wyscout_timestamps()
    xy.index = pd.to_datetime(xy.index)

    axes = {
        '1H': ax1,
        '2H': ax2}
    momenta = {}

    # 1st half

    xy_1half = xy[xy['matchPeriod'] == '1H']
    ball_x_1half = xy_1half['0.x'][xy_1half['ballInPlay']]
    momenta['1H'] = ball_x_1half.resample('1min').mean().rolling(5, min_periods=1).mean() / (tracab.pitch_dims[0] / 2)

    # 2nd half
    xy_2half = xy[xy['matchPeriod'] == '2H']
    ball_x_2half = xy_2half['0.x'][xy_2half['ballInPlay']]
    momenta['2H'] = ball_x_2half.resample('1min').mean().rolling(5, min_periods=1).mean() / (tracab.pitch_dims[0] / 2)

    for half in ['1H', '2H']:
        ax = axes[half]
        momentum = momenta[half]
        # ax.step(pd.to_datetime(momentum.index), momentum, where='pre', color='grey')
        ax.bar(pd.to_datetime(momentum.index)[momentum > 0], momentum[momentum > 0], width=pd.Timedelta(minutes=1), color=team_colors['Denmark'], alpha=0.8)
        ax.bar(pd.to_datetime(momentum.index)[momentum < 0], momentum[momentum < 0], width=pd.Timedelta(minutes=1), color=team_colors['Finland'], alpha=0.8)
        ax.axhline(0, color='grey', ls='--')
        # ax.fill_between(momentum.index, 0, momentum, where=(momentum > 0), color=team_colors['Denmark'], alpha=0.7, step='pre')
        # ax.fill_between(momentum.index, momentum, 0, where=(momentum < 0), color=team_colors['Finland'], alpha=0.7, step='pre')

    plt.suptitle("Game momentum", fontsize=title_fontsize)
    plt.savefig('graphs/momentum.png')
    plt.show()


def game_pace(tracab, wyscout):

    fig, (ax1, ax2) = game_timeline(wyscout, figsize=(17, 5), show=False)

    xy = tracab.XY_with_wyscout_timestamps()
    xy.index = pd.to_datetime(xy.index)
    xy = xy.resample('1S').first()
    xy = xy[xy['ballInPlay'] == 1]

    axes = {
        '1H': ax1,
        '2H': ax2}
    game_pace = {}

    # 1st half
    xy_1half = xy[xy['matchPeriod'] == '1H']
    game_pace_1half = np.sqrt(xy_1half['0.x'].diff()**2 + xy_1half['0.y'].diff()**2)/100
    game_pace['1H'] = game_pace_1half.resample('1min').mean()

    # 2nd half
    xy_2half = xy[xy['matchPeriod'] == '2H']
    game_pace_2half = np.sqrt(xy_2half['0.x'].diff()**2 + xy_2half['0.y'].diff()**2)/100
    game_pace['2H'] = game_pace_2half.resample('1min').mean()

    for half in ['1H', '2H']:
        ax = axes[half]
        pace = game_pace[half]
        ax.bar(pd.to_datetime(pace.index), pace, width=pd.Timedelta(minutes=1), color=team_colors['Finland'], alpha=0.8)

    plt.suptitle("Game pace", fontsize=title_fontsize)
    plt.savefig('graphs/game_pace.png')
    plt.show()


def penalty_statistics(goalkeeper, shooter, what='shots_faced'):

    goalkeeper, shooter = goalkeeper.copy(), shooter.copy()

    # our game corresponds to the following rows in the prepared data
    goalkeeper_match_index = 2
    shooter_match_index = 3

    if what not in ['shots_faced', 'dives', 'shots']:
        print(what + " cannot be plotted")
        return

    fig, ax = plt.subplots(1, 1, figsize=(17, 8))

    # goal_image = mpl.image.imread('luca/goal-with-grass.png')
    # ax.imshow(goal_image, alpha=0.5)
    # goal_borders = {'left': 84,
    #                 'right': 1370,
    #                 'bottom': 678,
    #                 'top': 59}
    goal_image = mpl.image.imread('luca/goal3.png')
    ax.imshow(goal_image, alpha=0.5)
    goal_borders = {'left': 357,
                    'right': 2527,
                    'bottom': 1175,
                    'top': 132}

    # update locations
    x_cols = [col for col in goalkeeper.columns if 'x' in col]
    y_cols = [col for col in goalkeeper.columns if 'y' in col]
    goalkeeper[x_cols] = goal_borders['left'] + goalkeeper[x_cols] * (goal_borders['right'] - goal_borders['left'])
    goalkeeper[y_cols] = goal_borders['bottom'] - goalkeeper[y_cols] * (goal_borders['bottom'] - goal_borders['top'])
    x_cols = [col for col in shooter.columns if 'x' in col]
    y_cols = [col for col in shooter.columns if 'y' in col]
    shooter[x_cols] = goal_borders['left'] + shooter[x_cols] * (goal_borders['right'] - goal_borders['left'])
    shooter[y_cols] = goal_borders['bottom'] - shooter[y_cols] * (goal_borders['bottom'] - goal_borders['top'])

    # ax.grid()

    if what == 'dives':
        # goalkeeper dives
        cmap = 'hot'
        sns.kdeplot(x=goalkeeper['Dive x norm'], y=goalkeeper['Dive y norm'], ax=ax,
                    clip=([goal_borders['left'], goal_borders['right']], [goal_borders['top'], goal_borders['bottom']]),
                    cmap=cmap, fill=True, alpha=0.7, cbar=False, bw_adjust=0.5)
        ax.scatter(goalkeeper.loc[goalkeeper_match_index, 'Dive x norm'],
                   goalkeeper.loc[goalkeeper_match_index, 'Dive y norm'],
                   s=1000, color='blue', alpha=0.6)

    elif what == 'shots_faced':
        # goalkeeper shots faced
        for i, shot in goalkeeper.iterrows():
            color = ['red', 'green'][int(shot['Goal'])]
            if i == goalkeeper_match_index:
                color = 'blue'
            marker = ['X', 'o'][int(shot['Goal'])]
            ax.scatter(shot['Shot x norm'], shot['Shot y norm'], s=1000, marker=marker, color=color, alpha=0.8)

    elif what == 'shots':
        # shooter past shot locations
        for i, shot in shooter.iterrows():
            color = ['red', 'green'][int(shot['Goal'])]
            if i == shooter_match_index:
                color = 'blue'
            marker = ['X', 'o'][int(shot['Goal'])]
            ax.scatter(shot['Shot x norm'], shot['Shot y norm'], s=1000, marker=marker, color=color, alpha=0.8)

    plt.axis('off')

    titles = {
        'shots_faced': 'Shots faced by Lukáš Hrádecký',
        'dives': 'Dive attempts by Lukáš Hrádecký',
        'shots': 'Previous attempts by Pierre-Emile Højbjerg'
    }
    ax.set_title(titles[what], pad=20, fontsize=title_fontsize)
    plt.savefig('graphs/penalty_stats_'+what+'.png')
    plt.show()


def pass_chain(wyscout, pass_chain, team, xvalue):

    fig, ax = draw_pitch(wyscout, orientation='vert')

    for i in range(0, len(pass_chain)):
        for j in range(0, len(pass_chain[i])):
            loc = pass_chain[i][j]
            if team == 'Denmark':
                color = team_colors[0]
                if j != len(pass_chain[i]) - 1:
                    plt.plot([pass_chain[i][j][1], pass_chain[i][j + 1][1]],
                             [pass_chain[i][j][0], pass_chain[i][j + 1][0]], linewidth=2, color=color,
                             alpha=xvalue[i])
            else:
                color = team_colors[1]
                if j != len(pass_chain[i]) - 1:
                    plt.plot([pass_chain[i][j][1], pass_chain[i][j + 1][1]],
                             [pass_chain[i][j][0], pass_chain[i][j + 1][0]], linewidth=2, color=color,
                             alpha=xvalue[i])
            ax.plot(loc[1], loc[0], marker='o', markersize=12, color=color, alpha=xvalue[i], ls='')
    ax.set_title("Passing Chains of "+team, pad=20, fontsize=title_fontsize)
    plt.savefig(f"graphs/Fabien/PassChain{team}.png")
    plt.show()


def tot_goals(wyscout, goalloc, pass1, pass2, pass3):

    fig, ax = draw_pitch(wyscout, orientation='vert')

    colors = ['#db2c2c', '#2e68c7', '#006400', '#FAC205']
    markersize = 12

    for i in range(0, len(goalloc)):
        loc = goalloc[i]
        goal = ax.plot(loc[1], loc[0], marker='o', markersize=markersize, color=colors[0], alpha=0.5, ls='', label='Goal')
    for i in range(0, len(pass1)):
        loc = pass1[i]
        pas1 = ax.plot(loc[1], loc[0], marker='o', markersize=markersize, color=colors[1], alpha=0.5, ls='', label='Assist')
    for i in range(0, len(pass2)):
        loc = pass2[i]
        pas2 = ax.plot(loc[1], loc[0], marker='o', markersize=markersize, color=colors[2], alpha=0.5, ls='', label='2nd Assist')
    for i in range(0, len(pass3)):
        loc = pass3[i]
        pas3 = ax.plot(loc[1], loc[0], marker='o', markersize=markersize, color=colors[3], alpha=0.5, ls='', label='3rd Assist')

    ax.legend(handles=[goal[0], pas1[0], pas2[0], pas3[0]])

    ax.set_title("Goals and where they came from", pad=20, fontsize=title_fontsize)
    plt.savefig(f"graphs/Fabien/Shotmap.png")
    plt.show()


def plot_defense(wyscout, location_list, team, word):

    fig, ax = draw_pitch(wyscout, orientation='vert')

    for i in range(0, len(location_list)):
        loc = location_list[i]
        ax.plot(loc[1], loc[0], marker='o', markersize=15, color=team_colors[team], alpha=0.8, ls='')

    ax.set_title(word + ' Defensive Actions of '+team, pad=20, fontsize=title_fontsize)
    plt.savefig(f"graphs/Fabien/Defense{team}{word}.png")
    plt.show()
    

def probability_heatmap(wyscout, goalloc, pass1, pass2, pass3):

    fig, ax = draw_pitch(wyscout, orientation='vert', half=False)

    # weights: goal=4, pass1=3, pass2=2, pass3=1
    locs = goalloc + goalloc + goalloc + goalloc + pass1 + pass1 + pass1 + pass2 + pass2 + pass3
    x = [loc[0] for loc in locs]
    y = [loc[1] for loc in locs]

    sns.kdeplot(x=y, y=x, ax=ax, clip=([0, 100], [0, 100]), bw_adjust=0.4, levels=10,
                cmap='viridis', fill=True, alpha=0.6, cbar=False)

    ax.set_title("Probability of Scoring in 4 steps or less", pad=20, fontsize=title_fontsize)
    plt.savefig("graphs/Fabien/OffHeatmap.png")
    plt.show()


def def_heatmap(wyscout, locs, team, success):

    fig, ax = draw_pitch(wyscout, orientation='vert')

    x = [loc[0] for loc in locs]
    y = [loc[1] for loc in locs]

    success_heatmaps = {
        'Successful': 'Greens',
        'Unsuccessful': 'Reds'
    }

    sns.kdeplot(x=y, y=x, ax=ax, clip=([0, 100], [0, 100]), bw_adjust=0.4, levels=10,
                cmap=success_heatmaps[success], fill=True, alpha=0.6, cbar=False)

    ax.set_title(success +' Defensive Heatmap of '+team, pad=20, fontsize=title_fontsize)
    plt.savefig(f"graphs/Fabien/DefHeatmap{team}{success}.png")
    plt.show()


def freekicks(wyscout):

    freekicks = wyscout.get_events_by_type('free_kick')

    arrow_props = dict(arrowstyle="simple",
                       shrinkA=0, shrinkB=0)

    for team in wyscout.teams:

        fig, ax = draw_pitch(wyscout, orientation='vert')

        for i, freekick in freekicks[freekicks['team.name'] == team].iterrows():
            start_loc = [freekick['location.x'], freekick['location.y']]
            end_loc = [freekick['pass.endLocation.x'], freekick['pass.endLocation.y']]
            if start_loc[0] > 50:
                alpha = 0.9
            else:
                alpha = 0.5

            ax.annotate("", xy=(end_loc[1], end_loc[0]), xytext=(start_loc[1], start_loc[0]),
                        arrowprops=dict(arrowstyle='-|>,head_width=0.5,head_length=1',
                                        color=team_colors[team],
                                        lw=5,
                                        ls='-',
                                        alpha=alpha))
            ax.plot(start_loc[1], start_loc[0], marker='o', markersize=12, ls='', color=team_colors[team], alpha=alpha)

        ax.set_title(team + "'s free kicks", pad=20, fontsize=title_fontsize)
        plt.savefig(f"graphs/Luca/free_kicks_{team}.png")
        plt.show()


def throwins(wyscout):

    throwins = wyscout.get_events_by_type('throw_in')

    for team in wyscout.teams:

        fig, ax = draw_pitch(wyscout, orientation='vert')

        for i, throwin in throwins[throwins['team.name'] == team].iterrows():
            start_loc = [throwin['location.x'], throwin['location.y']]
            end_loc = [throwin['pass.endLocation.x'], throwin['pass.endLocation.y']]
            if start_loc[0] > 50:
                alpha = 0.9
            else:
                alpha = 0.5

            ax.annotate("", xy=(end_loc[1], end_loc[0]), xytext=(start_loc[1], start_loc[0]),
                        arrowprops=dict(arrowstyle='-|>,head_width=0.5,head_length=1',
                                        color=team_colors[team],
                                        lw=5,
                                        ls='-',
                                        alpha=alpha))
            ax.plot(start_loc[1], start_loc[0], marker='o', markersize=12, ls='', color=team_colors[team], alpha=alpha)

        ax.set_title(team + "'s throw-ins", pad=20, fontsize=title_fontsize)
        plt.savefig(f"graphs/Luca/throw_ins_{team}.png")
        plt.show()
