import bz2
import pandas as pd
import numpy as np
import json
import xml.etree.ElementTree as ET
from time import time
from os.path import exists
import io
from datetime import timedelta


class Tracab:

    provider = 'tracab'
    pitch_dims = (2*5250, 2*3400)

    def __init__(self, path, team_names, competition_stage, xy_only=False):

        self.path = path
        self.teams = team_names
        self.competition_stage = competition_stage
        self.match_folder = path+competition_stage+'/'+team_names[0]+' v '+team_names[1]+'/'
        self.match_name = team_names[0]+' v '+team_names[1]

        tic = time()

        if xy_only and exists(self.match_folder+'xy.csv'):
            self.XY = pd.read_csv(self.match_folder + 'xy.csv', index_col=0, parse_dates=True, dtype={'ballInPlay': bool})
            self.obj_info = self.get_obj_info()
        else:
            match = self.load_match()
            self.matchID = int(match.attrib['id'])
            self.matchNr = int(match.attrib['matchNumber'])
            assert int(match[1].attrib['pitchLength']) == self.pitch_dims[0]
            assert int(match[1].attrib['pitchWidth']) == self.pitch_dims[1]
            self.phases = [self.Phase(phase) for phase in match[2]]
            self.frames = [self.Frame(frame) for frame in match[3]]
            self.standardize_data()
            self.obj_info = self.get_obj_info()
            self.XY = self.position_dfs()

        toc = time()
        print(f"Loading the Tracab data took {toc - tic} seconds.")

    def load_match(self):
        return ET.parse(bz2.open(self.match_folder+self.match_name+'.xml.bz2')).getroot()[0]

    def standardize_data(self):
        """Removes frames which lie outside the playing time
        and puts teams always on the same side of the pitch"""
        standardized_frames = []
        for frame in self.frames:
            for phase in self.phases:
                if phase.start <= frame.time <= phase.end:
                    if phase.leftTeamID != self.phases[0].leftTeamID:
                        for obj in frame.trackingObjs:
                            obj.x *= -1
                            obj.y *= -1
                    standardized_frames.append(frame)
                    break
        self.frames = standardized_frames

    class Phase:
        def __init__(self, phase):
            self.start = phase.attrib['start']
            self.end = phase.attrib['end']
            self.leftTeamID = int(phase.attrib['leftTeamID'])

    class Frame:
        def __init__(self, frame):
            self.time = frame.attrib['utc']
            self.ballInPlay = frame.attrib['isBallInPlay']
            self.ballPossession = frame.attrib['ballPossession']
            self.trackingObjs = [self.TrackingObj(obj) for obj in frame[0]]

        class TrackingObj:
            def __init__(self, obj):
                self.type = obj.attrib['type']
                self.id = obj.attrib['id']
                self.x = int(obj.attrib['x'])
                self.y = int(obj.attrib['y'])
                self.sampling = obj.attrib['sampling']

    def get_obj_info(self):
        if exists(self.match_folder+'obj_info.json'):
            f = io.open(self.match_folder + 'obj_info.json', mode="r", encoding="utf-8")
            obj_info = json.load(f)
        else:
            obj_info = {}
            for frame in self.frames:
                for obj in frame.trackingObjs:
                    if obj.id not in obj_info.keys():
                        obj_info[obj.id]['type'] = obj.type
                    else:
                        continue
            with open(self.match_folder+'obj_info.json', 'w') as f:
                json.dump(obj_info, f, indent=4)
        return obj_info

    def position_dfs(self):

        columns = ['time', 'ballInPlay', 'ballPossession'] + \
                  [id+'.x' for id in self.obj_info.keys()] + \
                  [id+'.y' for id in self.obj_info.keys()]
        index = list(range(len(self.frames)))

        xy = pd.DataFrame(columns=columns, index=index)

        for i, frame in enumerate(self.frames):
            xy.at[i, 'time'] = frame.time
            xy.at[i, 'ballInPlay'] = frame.ballInPlay
            xy.at[i, 'ballPossession'] = frame.ballPossession
            for obj in frame.trackingObjs:
                xy.at[i, obj.id+'.x'] = obj.x
                xy.at[i, obj.id+'.y'] = obj.y

        xy.to_csv(self.match_folder+'xy.csv', index=True)
        xy = pd.read_csv(self.match_folder + 'xy.csv', index_col=0, parse_dates=True, dtype={'ballInPlay': bool})
        return xy

    def average_positions(self):
        avg_pos = {}
        for id in self.obj_info:
            avg_x = self.XY[id+'.x'].loc[self.XY['ballInPlay']].mean()
            avg_y = self.XY[id+'.y'].loc[self.XY['ballInPlay']].mean()
            avg_pos[id] = [avg_x, avg_y]
        return avg_pos

    def distance_covered(self, start=None, end=None, ballInPlay=False, sampling_sec=1, normalize90min=True):

        distances = {'Denmark (tot)': 0, 'Finland (tot)': 0}

        xy = self.XY.loc[pd.to_datetime(start):pd.to_datetime(end)]
        if ballInPlay:
            xy = xy[xy['ballInPlay']]
        loc_cols = [col for col in self.XY.columns if '.x' in col or '.y' in col]
        xy = xy[loc_cols].resample(str(sampling_sec)+'S').first()
        pos_diff = xy.diff()

        for id in self.obj_info:
            distance = np.sum(np.sqrt(pos_diff[id+'.x']**2 + pos_diff[id+'.y']**2)/100000)
            distances[self.obj_info[id]['name']] = distance
            if self.obj_info[id]['type'] == '0':
                distances['Denmark (tot)'] += distance
            elif self.obj_info[id]['type'] == '1':
                distances['Finland (tot)'] += distance

        if normalize90min:
            for id in self.obj_info:
                distances[self.obj_info[id]['name']] *= 90 / (xy[id+'.x'].count()*sampling_sec/60)

        return distances

    def velocities_percentile(self, start=None, end=None, ballInPlay=False, sampling_sec=1, percentile=50):

        velocities = {}

        xy = self.XY.loc[pd.to_datetime(start):pd.to_datetime(end)]
        if ballInPlay:
            xy = xy[xy['ballInPlay']]
        loc_cols = [col for col in self.XY.columns if '.x' in col or '.y' in col]
        xy = xy[loc_cols].resample(str(sampling_sec)+'S').first()
        pos_diff = xy.diff()

        for id in self.obj_info:
            velocities[self.obj_info[id]['name']] = \
                np.nanpercentile(np.sqrt(pos_diff[id+'.x']**2 + pos_diff[id+'.y']**2)/100, q=percentile)/sampling_sec

        return velocities

    def XY_with_wyscout_timestamps(self):
        """This function is hardcoded to suit our specific game!"""
        if exists(self.match_folder+'xy_wyscout_timestamps.csv'):
            xy = pd.read_csv(self.match_folder+'xy_wyscout_timestamps.csv', index_col=0, dtype={'ballInPlay': bool})
        else:
            xy = self.XY
            xy_1half = xy.loc[:'2021-06-12T18:30:59.234Z'].copy()
            xy_1half['matchPeriod'] = '1H'
            xy_2half = xy.loc['2021-06-12T18:30:59.234Z':].copy()
            xy_2half['matchPeriod'] = '2H'
            index_1half = xy_1half.index.to_series() - pd.to_datetime('2021-06-12T16:00:16.194Z')
            index_2half = xy_2half.index.to_series() - pd.to_datetime('2021-06-12T18:30:59.234Z') + pd.Timedelta(minutes=45)

            xy = pd.concat((xy_1half, xy_2half))
            index = pd.concat((index_1half, index_2half))
            index = index.apply(lambda td: str(timedelta(seconds=td.total_seconds())))
            xy = xy.set_index(index)
            xy.to_csv(self.match_folder+'xy_wyscout_timestamps.csv', index=True)

        return xy
