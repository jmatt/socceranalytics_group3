#----------------------------------------------------------------------------
# Created By: Danny Camenisch (dcamenisch)
# Created Date: 10/03/2022
# version ='1.1'
# ---------------------------------------------------------------------------
""" 
Simple module to convert a xml file containing data in TRACAB format to a Match object
"""
# ---------------------------------------------------------------------------
import xml.etree.ElementTree as et
import numpy as np

class Match:
    def __init__(self, filePath):
        match = et.parse(filePath).getroot()[0]

        self.matchID      = int(match.attrib['id'])
        self.matchNr      = int(match.attrib['matchNumber'])
        self.date         = match.attrib['dateMatch']
        self.stadiumID    = int(match[1].attrib['id'])
        self.stadiumName  = match[1].attrib['name']
        self.pitchLength  = int(match[1].attrib['pitchLength'])
        self.pitchWidth   = int(match[1].attrib['pitchWidth'])
        self.phases       = [Phase(phase) for phase in match[2]]
        self.frames       = [Frame(frame) for frame in match[3]]

        self.removeExcessFrames()
        
        
    def removeExcessFrames(self):
        keep = []
        for frame in self.frames:
            for phase in self.phases:
                if frame.time >= phase.start and frame.time <= phase.end:
                    keep.append(frame)
                    break

        self.frames = keep
 
class Phase:
    def __init__(self, phase):
        self.start       = phase.attrib['start']
        self.end         = phase.attrib['end']
        self.leftTeamID  = int(phase.attrib['leftTeamID'])
        
class Frame:
    def __init__(self, frame):
        self.time            = frame.attrib['utc']
        self.ballInPlay      = frame.attrib['isBallInPlay']
        self.ballPossession  = frame.attrib['ballPossession']
        self.trackingObjs    = [TrackingObj(obj) for obj in frame[0]]
    
class TrackingObj:
    def __init__(self, obj):
        self.type      = obj.attrib['type']
        self.id        = obj.attrib['id']
        self.x         = int(obj.attrib['x'])
        self.y         = int(obj.attrib['y'])
        self.sampling  = obj.attrib['sampling']

def get_ball_possession (match):
    ball_possession=[]
    num_frames=len(match.frames)
    for f in range(num_frames):
        if(match.frames[f].ballInPlay=="0"):
          ball_possession.append(0)
        else:
          possession_string=match.frames[f].ballPossession
          if(possession_string == "Home"):
            ball_possession.append(1)
          elif(possession_string == "Away"):
            ball_possession.append(-1)
          else:
            ball_possession.append(0)
    return(np.asarray(ball_possession))

def get_first_appearance(match,id):
    """
    Snippet $423
    """
    num_frames = len(match.frames)
    for f in range(0,num_frames):
        for p in match.frames[f].trackingObjs:
            if((p.id) == id):
                return f

def get_last_appearance(match,id):
    num_frames = len(match.frames)
    for f in range(num_frames-1, 0, -1):
        for p in match.frames[f].trackingObjs:
            if((p.id) == id):
                return f+1

from datetime import datetime, timedelta

def get_cut_indices (match):
  """
We decided to cut out the frames after the accident of Eriksen happened.
This function searches the indices of the frames, that need to be cut out

Returns: Vector with elements 1,-1,0
  1: take the position of this frame
  0: don't take the position of this frame
  -1: reverse the position of this frame

We use the minute that was visible in the TV and the appearence of Jensen
to find the right indices

THIS FUNCTION CAN NOT BE USED FOR OTHER MATCHES!
  """
  num_frames=len(match.frames)
  #Convert the time from TV
  in_time='42:11' #The minute:seconde where the accident happened
  in_time_stamp= datetime.strptime(in_time, '%M:%S') 
  input_game_time=timedelta(minutes=in_time_stamp.minute, seconds=in_time_stamp.second)
  start_game = datetime.strptime(match.phases[0].start, '%Y-%m-%dT%H:%M:%S.%f') 
  datetimeObj = datetime.strptime(match.frames[0].time, '%Y-%m-%dT%H:%M:%S.%fZ') 
  played_time=datetimeObj-start_game

  #Find 1st index
  count_1=0
  while(played_time<input_game_time):
    count_1+=1
    datetimeObj = datetime.strptime(match.frames[count_1].time, '%Y-%m-%dT%H:%M:%S.%fZ') 
    played_time=datetimeObj-start_game

  #Find 2nd index
  jensen_id="250080451"
  count_2=get_first_appearance(match, jensen_id) #Snippet $423

  #Find 3rd index
  end_1st = datetime.strptime(match.phases[0].end, '%Y-%m-%dT%H:%M:%S.%f') 
  datetimeObj = datetime.strptime(match.frames[count_2].time, '%Y-%m-%dT%H:%M:%S.%fZ') 
  
  count_3=count_2
  while(datetimeObj<end_1st):
    count_3+=1
    datetimeObj = datetime.strptime(match.frames[count_3].time, '%Y-%m-%dT%H:%M:%S.%fZ') 
  count_3-=1 #to correct

  #Find 4th index
  begin_2nd = datetime.strptime(match.phases[1].start, '%Y-%m-%dT%H:%M:%S.%f') 
  datetimeObj = datetime.strptime(match.frames[count_3].time, '%Y-%m-%dT%H:%M:%S.%fZ') 
  
  count_4=count_3
  while(datetimeObj<=begin_2nd):
    count_4+=1
    datetimeObj = datetime.strptime(match.frames[count_4].time, '%Y-%m-%dT%H:%M:%S.%fZ') 

  return([*np.ones(count_1), *np.zeros(count_2-count_1), *np. ones(count_3-count_2), *(np.zeros(num_frames-count_3)-np.ones(num_frames-count_3))])
    
from matplotlib import pyplot as plt
from matplotlib.axes import Axes
from matplotlib.patches import Arc
from matplotlib.transforms import Affine2D


def draw_pitch(
    ax: Axes,
    pitch_center: tuple = (0, 34),
    pitch_length: float = 105,
    pitch_width: float = 68,
    linewidth: float = 1.2,
    linecolor="black",
    background_color=None,
    zorder: int = -10,
    orient_vertical: bool = False,
):
    """Draw a football pitch on a given axes.

    The pitch is fitted according to the provided center and width/length arguments.
    Scale is not guaranteed.


    Parameters
    ----------
    ax : matplotlib.axes.Axes
        Axes to draw the pitch on
    pitch_center : tuple
        Center of the pitch, by default (0, 34). The center is the point in the
        middle of the pitch, lengthwise and widthwise respectively. If orient_vertical
        is False (default), this translates to x and y axes.
    pitch_length : float
        Length of the pitch, by default 105
    pitch_width : float
        Width of the pitch, by default 68
    linewidth : float
        Width of the lines, passed to plot calls and patch initializations, by default 1.2
    linecolor : color
        Color of the lines, passed to plot calls and patch initializations, by default "black"
    background_color : color
        Color of the plot background as a matplotlib color, by default None
    zorder : int, optional
        Plotting order of the pitch on the axes, by default -10
    orient_vertical : bool, optional
        Change the pitch orientation to vertical, by default False
    """
    if orient_vertical:
        transform = Affine2D().rotate_deg(90).scale(-1, 1) + ax.transData
    else:
        transform = ax.transData

    x = lambda x: (x / 130) * pitch_length + pitch_center[0] - pitch_length / 2
    y = lambda y: (y / 90) * pitch_width + pitch_center[1] - pitch_width / 2

    rat_x = pitch_length / 130
    rat_y = pitch_width / 90

    plot_arguments = dict(
        color=linecolor, zorder=zorder, transform=transform, linewidth=linewidth
    )

    # Pitch Outline & Centre Line
    ax.plot([x(0), x(0)], [y(0), y(90)], **plot_arguments)
    ax.plot([x(0), x(130)], [y(90), y(90)], **plot_arguments)
    ax.plot([x(130), x(130)], [y(90), y(0)], **plot_arguments)
    ax.plot([x(130), x(0)], [y(0), y(0)], **plot_arguments)
    ax.plot([x(65), x(65)], [y(0), y(90)], **plot_arguments)

    # Left Penalty Area
    ax.plot([x(16.5), x(16.5)], [y(65), y(25)], **plot_arguments)
    ax.plot([x(0), x(16.5)], [y(65), y(65)], **plot_arguments)
    ax.plot([x(16.5), x(0)], [y(25), y(25)], **plot_arguments)

    # Right Penalty Area
    ax.plot([x(130), x(113.5)], [y(65), y(65)], **plot_arguments)
    ax.plot([x(113.5), x(113.5)], [y(65), y(25)], **plot_arguments)
    ax.plot([x(113.5), x(130)], [y(25), y(25)], **plot_arguments)

    # Left 6-yard Box
    ax.plot([x(0), x(5.5)], [y(54), y(54)], **plot_arguments)
    ax.plot([x(5.5), x(5.5)], [y(54), y(36)], **plot_arguments)
    ax.plot([x(5.5), x(0.5)], [y(36), y(36)], **plot_arguments)

    # Right 6-yard Box
    ax.plot([x(130), x(124.5)], [y(54), y(54)], **plot_arguments)
    ax.plot([x(124.5), x(124.5)], [y(54), y(36)], **plot_arguments)
    ax.plot([x(124.5), x(130)], [y(36), y(36)], **plot_arguments)

    # Prepare circles
    centre_circle = plt.Circle((x(65), y(45)), 9.15, fill=False, **plot_arguments)
    centre_spot = plt.Circle((x(65), y(45)), linewidth / 2, **plot_arguments)
    left_pen_spot = plt.Circle((x(11), y(45)), linewidth / 4, **plot_arguments)
    right_pen_spot = plt.Circle((x(119), y(45)), linewidth / 4, **plot_arguments)

    # Draw Circles
    ax.add_patch(centre_circle)
    ax.add_patch(centre_spot)
    ax.add_patch(left_pen_spot)
    ax.add_patch(right_pen_spot)

    # Prepare Arcs
    left_arc = Arc(
        (x(11), y(45)),
        height=18.3 * rat_y,
        width=18.3 * rat_x,
        angle=0,
        theta1=312,
        theta2=48,
        **plot_arguments,
    )
    right_arc = Arc(
        (x(119), y(45)),
        height=18.3 * rat_y,
        width=18.3 * rat_x,
        angle=0,
        theta1=128,
        theta2=232,
        **plot_arguments,
    )

    # Draw Arcs
    ax.add_patch(left_arc)
    ax.add_patch(right_arc)

    if background_color is not None:
        ax.set_facecolor(background_color)

import numpy as np
#Get a list with player id's

def get_substitution_vec (match):
  num_frames=len(match.frames)
  player_id_begin=[]
  for i in range(1,23):
    player_id_begin.append(match.frames[0].trackingObjs[i].id)

  player_id_end=[]
  for i in range(1,23):
    player_id_end.append(match.frames[num_frames-1].trackingObjs[i].id)

  out_going=[]
  in_coming=[]
  sub_frames=[]
  for i in player_id_begin:
    if (i not in player_id_end):
      sub_frames.append(get_last_appearance(match, i))
      out_going.append(i)
  
  ex_frames=[]
  for i in player_id_end:
    if (i not in player_id_begin):
      ex_frames.append(get_first_appearance(match, i))
      in_coming.append(i)

  p = np.asarray(sub_frames).argsort()
  out_going=np.asarray(out_going)[p]
  k = np.asarray(ex_frames).argsort()
  in_coming=np.asarray(in_coming)[k]

  return(out_going, in_coming)

def get_player_id (match):
  home_id=[]
  away_id=[]
  for i in range(1,23):
    if(match.frames[0].trackingObjs[i].type=="0"):
      home_id.append(match.frames[0].trackingObjs[i].id)
    elif(match.frames[0].trackingObjs[i].type=="1"):
      away_id.append(match.frames[0].trackingObjs[i].id)
  return(home_id, away_id)


