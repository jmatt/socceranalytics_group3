import json
import bz2
import pandas as pd
import numpy as np
from datetime import datetime, timedelta


class Statsbomb:

    provider = 'statsbomb'
    pitch_dims = (120, 80)

    def __init__(self, path, match_id, team_names, competition_stage):
        self.path = path
        self.teams = team_names
        self.competition_stage = competition_stage

        self.match_id = self.match()['match_id']
        assert self.match_id == match_id

        self.events = self.load_events()
        self.event_types = pd.unique([event['type']['name'] for event in self.events])

        self.shots = self.get_event_df('Shot')
        self.passes = self.get_event_df('Pass')

    def match(self):
        matches = json.load(open(self.path + "matches.json", encoding='utf-8'))
        match = [m for m in matches if
                 m["home_team"]["home_team_name"] == self.teams[0] and
                 m["away_team"]["away_team_name"] == self.teams[1] and
                 m["competition_stage"]["name"] == self.competition_stage][0]
        return match

    def load_events(self):
        return json.load(bz2.open(self.path + "events/" + str(self.match_id) + ".json.bz2"))

    def formations_lineups(self):
        starting_xis = [self.events[0]["tactics"], self.events[1]["tactics"]]
        formations = [xi["formation"] for xi in starting_xis]
        lineups = [xi["lineup"] for xi in starting_xis]

        players, numbers, positions = [None, None], [None, None], [None, None]

        # home team
        players[0] = [player['player']['name'] for player in lineups[0]]
        numbers[0] = [player['jersey_number'] for player in lineups[0]]
        positions[0] = [player['position']['name'] for player in lineups[0]]
        # away team
        players[1] = [player['player']['name'] for player in lineups[1]]
        numbers[1] = [player['jersey_number'] for player in lineups[1]]
        positions[1] = [player['position']['name'] for player in lineups[1]]

        lineups = []
        lineups.append(pd.DataFrame(list(map(list, zip(*[positions[0], players[0], numbers[0]]))), columns=['Position', 'Name', 'Jersey Number']))
        lineups.append(pd.DataFrame(list(map(list, zip(*[positions[1], players[1], numbers[1]]))), columns=['Position', 'Name', 'Jersey Number']))

        return formations, lineups

    def xgoals_by_team(self):
        return tuple(np.sum(self.shots['shot.statsbomb_xg'][self.shots['team.name'] == team]) for team in self.teams)

    def get_event_df(self, event_type: str):
        return pd.json_normalize([event for event in self.events if event['type']['name'] == event_type])

    def match_stats(self):

        stats = pd.DataFrame(index=['Goals',
                                    'Possession (%)',
                                    'Total Attempts',
                                    'Attempts on target',
                                    'Corners taken',
                                    'Passing accuracy (%)',
                                    'Passes completed',
                                    'Passes attempted',
                                    'Balls recovered',
                                    'Blocks',
                                    'Offsides',
                                    'Saves',
                                    'Distance covered (km)',
                                    'Fouls committed',
                                    'Yellow cards',
                                    'Red cards'],
                             columns=['Denmark', 'Finland'])

        teams = stats.columns

        # shots
        shots = self.shots

        stats.loc['Goals'] = [((shots['team.name'] == team) & (shots['shot.outcome.name'] == 'Goal')).sum() for team in teams]
        stats.loc['Total Attempts'] = [np.sum(shots['team.name'] == team) for team in teams]
        stats.loc['Attempts on target'] = [((shots['team.name'] == team) & ((shots['shot.outcome.name'] == 'Goal') | (shots['shot.outcome.name'] == 'Saved'))).sum() for team in teams]

        # passes
        passes = self.passes

        stats.loc['Passes completed'] = [((passes['team.name'] == team) & (pd.isna(passes['pass.outcome.name']))).sum() for team in teams]
        stats.loc['Passes attempted'] = [(passes['team.name'] == team).sum() for team in teams]
        stats.loc['Passing accuracy (%)'] = stats.loc['Passes completed'] / stats.loc['Passes attempted'] * 100

        stats.loc['Offsides'] = [((passes['team.name'] == team) & (passes['pass.outcome.name'] == 'Pass Offside')).sum() for team in teams]
        stats.loc['Corners taken'] = [((passes['team.name'] == team) & (passes['pass.type.name'] == 'Corner')).sum() for team in teams]

        ball_recoveries = self.get_event_df('Ball Recovery')
        stats.loc['Balls recovered'] = [(ball_recoveries['team.name'] == team).sum() for team in teams]

        # fouls
        fouls_committed = self.get_event_df('Foul Committed')
        stats.loc['Fouls committed'] = [(fouls_committed['team.name'] == team).sum() for team in teams]
        stats.loc['Yellow cards'] = [((fouls_committed['team.name'] == team) & (fouls_committed['foul_committed.card.name'] == 'Yellow Card')).sum() for team in teams]
        stats.loc['Red cards'] = [((fouls_committed['team.name'] == team) & (fouls_committed['foul_committed.card.name'] == 'Red Card')).sum() for team in teams]

        # goalkeeper
        goalkeeper = self.get_event_df('Goal Keeper')
        stats.loc['Saves'] = [((goalkeeper['team.name'] == team) & (goalkeeper['goalkeeper.type.name'].str.contains('Saved'))).sum() for team in teams]

        blocks = self.get_event_df('Block')
        stats.loc['Blocks'] = [(blocks['team.name'] == team).sum() for team in teams]

        # possession
        possession_durations = {'Denmark': timedelta(), 'Finland': timedelta()}
        possession_counts = {'Denmark': 0, 'Finland': 0}
        current_possession = 1
        current_team = self.events[2]['possession_team']['name']
        period = 1
        possession_start = datetime.strptime(self.events[0]['timestamp'], "%H:%M:%S.%f")   # yields 00:00:00

        for i, event in enumerate(self.events[2:]):

            if event['period'] > period:
                # 2nd half started
                period = event['period']
                possession_start = datetime.strptime(event['timestamp'], "%H:%M:%S.%f")
                continue

            if event['possession'] > current_possession:

                duration_last_possession = datetime.strptime(event['timestamp'], "%H:%M:%S.%f") - possession_start
                assert duration_last_possession >= timedelta()

                possession_durations[current_team] = duration_last_possession
                possession_counts[current_team] += 1

                current_possession = event['possession']
                current_team = event['possession_team']['name']
                possession_start = datetime.strptime(event['timestamp'], "%H:%M:%S.%f")

        stats.loc['Possession duration (%)'] = [possession_durations[team]/(possession_durations['Denmark']+possession_durations['Finland']) for team in teams]
        stats.loc['Possession (%)'] = [possession_counts[team]/sum(possession_counts.values()) for team in teams]

        return stats

