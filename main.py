from statsbomb import Statsbomb
from wyscout import Wyscout
from tracab import Tracab
import plotting as plt
import pandas as pd
import os
from wyscout import Wyscout_all
import funcFabien as ff

INFO = {
    'statsbomb': {
        'path': "data/statsbomb360/",
        'match_id': 3788742
    },
    'wyscout': {
        'path': "data/wyscout/",
        'match_id': 5034297
    },
    'tracab': {
        'path': "data/tracab/",
        'match_id': None
    },
    'team_names': ['Denmark', 'Finland'],
    'competition_stage': 'Group Stage'
}


# --- STATSBOMB DATA --- #

statsbomb = Statsbomb(INFO['statsbomb']['path'],
                      INFO['statsbomb']['match_id'],
                      INFO['team_names'],
                      INFO['competition_stage'])
match = statsbomb.match()

score = (match["home_score"], match["away_score"])
referee = match["referee"]["name"]
location = (match["stadium"]["name"], match["stadium"]["country"]["name"])
time = (match["match_date"], match["kick_off"])
print(score, referee, location, time)
print("# Events:", len(statsbomb.events))
print("# Passes:", len(statsbomb.passes.index))
print("# Shots:", len(statsbomb.shots.index))
print('Statsbomb xGoals:', statsbomb.xgoals_by_team())

plt.starting_xi(statsbomb)


# --- WYSCOUT DATA --- #

wyscout = Wyscout(INFO['wyscout']['path'],
                  INFO['wyscout']['match_id'],
                  INFO['team_names'],
                  INFO['competition_stage'])

plt.game_events(wyscout)

print("# Events:", len(wyscout.events))
print("# Passes:", len(wyscout.passes.index))
print("# Shots:", len(wyscout.shots.index))
xgoals = wyscout.xgoals_by_team()
print('xGoals:', xgoals)

plt.xgoals_racechart(wyscout)
plt.shot_map(wyscout, show_xg='size')

plt.action_map(wyscout, 'C. Eriksen')
plt.action_map(wyscout, 'M. Jensen')
plt.player_passes(wyscout, 'C. Eriksen')
plt.player_passes(wyscout, 'M. Jensen')

# Set pieces
plt.freekicks(wyscout)
plt.throwins(wyscout)

# --- TRACAB DATA --- #

tracab = Tracab(INFO['tracab']['path'],
                INFO['team_names'],
                INFO['competition_stage'],
                xy_only=True)

plt.game_momentum(tracab, wyscout)
plt.game_pace(tracab, wyscout)

# Freeze frames
# Kickoff
plt.freeze_frame(tracab, 'kickoff', '2021-06-12T16:00:16.194Z', convex_hulls=['Denmark', 'Finland'])
# Eriksen incident
plt.freeze_frame(tracab, name='eriksen_incident', time='2021-06-12T16:42:20.274Z')
# Goal
plt.freeze_frame(tracab, name='goal', time='2021-06-12T18:54:21.514Z')

# Animate scenes
animate_scenes = {
    # 'kickoff': {'timestamp': ['2021-06-12T16:00:16.194Z', '2021-06-12T16:00:30.194Z'], 'convex_hulls': []},
    'eriksen_incident': {'timestamp': ['2021-06-12T16:42:20.274Z', '2021-06-12T16:42:50.274Z'], 'convex_hulls': []},
    # 'goal': {'timestamp': ['2021-06-12T18:53:52.194Z', '2021-06-12T18:54:21.554Z'], 'convex_hulls': []},
    # 'defense_finland1': {'timestamp': '00:14:45', 'duration': 30, 'convex_hulls': ['Finland']},
    # 'defense_finland2': {'timestamp': '00:29:15', 'duration': 30, 'convex_hulls': ['Finland']},
    # 'defense_finland3': {'timestamp': '00:39:03', 'duration': 30, 'convex_hulls': ['Finland']},
    # 'defense_finland4': {'timestamp': '00:48:02', 'duration': 30, 'convex_hulls': ['Finland']},
    # 'defense_finland5': {'timestamp': '01:08:44', 'duration': 30, 'convex_hulls': ['Finland']},
    # 'defense_finland6': {'timestamp': '01:21:05', 'duration': 60, 'convex_hulls': ['Finland']},
    # 'defense_denmark1': {'timestamp': '00:12:49', 'duration': 22, 'convex_hulls': ['Denmark']},
    # 'defense_denmark2': {'timestamp': '00:37:20', 'duration': 60, 'convex_hulls': ['Denmark']},
    # 'defense_denmark3': {'timestamp': '00:53:40', 'duration': 60, 'convex_hulls': ['Denmark']},
    # 'defense_denmark4': {'timestamp': '00:55:20', 'duration': 60, 'convex_hulls': ['Denmark']},
}
for scene in animate_scenes:
    if not isinstance(animate_scenes[scene]['timestamp'], list):
        game_time = pd.Timedelta(animate_scenes[scene]['timestamp'])
        duration = animate_scenes[scene]['duration']
        if game_time.total_seconds()/60 <= 45:
            # 1st half
            start = pd.to_datetime('2021-06-12T16:00:16.194Z') + game_time + pd.Timedelta(seconds=-duration//2)
            end = pd.to_datetime('2021-06-12T16:00:16.194Z') + game_time + pd.Timedelta(seconds=duration//2)
        else:
            # 2nd half
            start = pd.to_datetime('2021-06-12T18:30:59.234Z') + game_time + pd.Timedelta(minutes=-45, seconds=-duration//2)
            end = pd.to_datetime('2021-06-12T18:30:59.234Z') + game_time + pd.Timedelta(minutes=-45, seconds=duration//2)
    else:
        start, end = animate_scenes[scene]['timestamp'][0], animate_scenes[scene]['timestamp'][1]

    plt.animate_tracab(tracab, name=scene, start=start, end=end, convex_hulls=animate_scenes[scene]['convex_hulls'])

plt.real_starting_xi(tracab, shift_teams=True)
plt.players_running(tracab)
plt.player_heatmap(tracab, "Christian Dannemann Eriksen", method='kde')
plt.player_heatmap(tracab, "Mathias Jensen", method='kde')


# --- MATCH STATISTICS --- #

plt.match_statistics(wyscout, tracab, source='data')
plt.match_statistics(wyscout, tracab, source='UEFA')


# --- PENALTY --- #

goalkeeper = pd.read_csv('luca/goalie_pens.csv', header=0, delimiter=';')
shooter = pd.read_csv('luca/shooter_pens.csv', header=0, delimiter=';')
plt.penalty_statistics(goalkeeper, shooter, what='shots_faced')
plt.penalty_statistics(goalkeeper, shooter, what='dives')
plt.penalty_statistics(goalkeeper, shooter, what='shots')

# --- EVENT VIDEOS --- #

timestamps = {'goal': 3960,
              'eriksen_incident': 2540,
              'penalty': 4850}

for clip in timestamps:
    if clip in ['penalty']:
        print("Creating clip", clip)
        call_eventvideo = 'python data/wyscout/eventvideo/eventvideo.py 5034297 ' + str(timestamps[clip]) + \
                          ' graphs/eventvideos/' + clip + '.mp4'
        os.system(call_eventvideo)
        os.system('ffmpeg -loglevel quiet -i graphs/eventvideos/' + clip + '.mp4 -vf scale=-2:720 ' + 'graphs/eventvideos/' + clip + '_small.mp4')


# --- SPECIAL OFFENSE / DEFENSE --- #

wyscout_all = Wyscout_all("data/wyscout/", 5034297)
teams = ['Denmark', 'Finland']
defensive_locs = {'Successful': ff.defense,
                 'Unsuccessful': ff.baddefense
}

match_ids = []
for i in range(5034291, 5034315):
    match_ids.append(i)
for i in range(5111384, 5111411):
    match_ids.append(i)

goalloc, pass1, pass2, pass3 = ff.goalsnassists(match_ids)
goal_probabilities = ff.probab(*ff.goalsnassists(match_ids))
plt.tot_goals(wyscout_all, goalloc, pass1, pass2, pass3)
plt.probability_heatmap(wyscout_all, goalloc, pass1, pass2, pass3)

for team in teams:
    pass_chain = ff.relevantpasses(team, wyscout_all)
    xvalue = ff.xvalue(goal_probabilities, pass_chain)
    plt.pass_chain(wyscout_all, pass_chain, team, xvalue)
    for success in defensive_locs:
        n_all_def_actions = len(ff.defense(wyscout_all, team)) + len(ff.baddefense(wyscout_all, team))
        locs = defensive_locs[success](wyscout_all, team)
        plt.plot_defense(wyscout_all, locs, team, success)
        plt.def_heatmap(wyscout_all, locs, team, success)
