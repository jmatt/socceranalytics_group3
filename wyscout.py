import json
from zipfile import ZipFile
import os.path
import pandas as pd
import numpy as np


class Wyscout:

    provider = 'wyscout'
    pitch_dims = (100, 100)

    def __init__(self, path, match_id, team_names, competition_stage):
        self.path = path
        self.teams = team_names
        self.competition_stage = competition_stage

        self.match_id = self.match()['matchId']
        assert self.match_id == match_id

        self.events = self.load_events()
        self.event_types = pd.unique([event['type']['primary'] for event in self.events])
        self.secondary_event_types = pd.unique([sec for event in self.events for sec in event['type']['secondary']])

        self.shots = self.get_events_by_type('shot')
        self.passes = self.get_events_by_type('pass')
        self.fouls = self.get_events_by_type('infraction')

        self.matchstats = self.matchstats()
        self.playerstats = self.playerstats()

    def match(self):

        competition_stage_id = {'Group Stage': 4421438, 'Round of 16': 4421439, 'Quarterfinal': 4421440,
                                'Semi-Final': 4421441, 'Final': 4421442}[self.competition_stage]
        matches = json.load(open(self.path + "matches.json", encoding='utf-8'))['matches']
        match = [m for m in matches if
                 self.teams[0]+' - '+self.teams[1] in m['label'] and
                 m["roundId"] == competition_stage_id][0]
        return match

    def load_events(self):
        file = self.path + str(self.match_id) + "_events.json"
        if not os.path.isfile(file):
            with ZipFile(self.path + str(self.match_id) + ".zip", 'r') as f:
                f.extractall(self.path)
        return json.load(open(file))['events']

    def get_timestamps_periods(self):
        return {
            'start_1H': '00:00:00.000',
            'start_2H': '00:45:00.000',
            'end_1H': [event['matchTimestamp'] for event in self.events if event['matchPeriod'] == '1H'][-1],
            'end_2H': [event['matchTimestamp'] for event in self.events if event['matchPeriod'] == '2H'][-1]
        }

    def playerstats(self):
        file = self.path + str(self.match_id) + "_playerstats.json"
        if not os.path.isfile(file):
            with ZipFile(self.path + str(self.match_id) + ".zip", 'r') as f:
                f.extractall(self.path)
        playerstats = json.load(open(file))['players']
        playerstats = pd.json_normalize(playerstats)
        return playerstats

    def matchstats(self):
        file = self.path + str(self.match_id) + "_matchstats.json"
        if not os.path.isfile(file):
            with ZipFile(self.path + str(self.match_id) + ".zip", 'r') as f:
                f.extractall(self.path)
        matchstats = pd.json_normalize(json.load(open(file)))

        den_cols = [c for c in matchstats.columns if '7712' in c]
        fin_cols = [c for c in matchstats.columns if '4849' in c]
        assert len(den_cols) == len(fin_cols)
        den_stats = matchstats[den_cols].squeeze()
        den_stats = den_stats.set_axis([idx.replace('7712.', '') for idx in den_stats.index])
        fin_stats = matchstats[fin_cols].squeeze()
        fin_stats = fin_stats.set_axis([idx.replace('4849.', '') for idx in fin_stats.index])
        stats = pd.DataFrame({'Denmark': den_stats,
                              'Finland': fin_stats})

        return stats

    def get_events_by_type(self, event_type: str):
        return pd.json_normalize([event for event in self.events if event['type']['primary'] == event_type])

    def get_events_by_player(self, player_name: str):
        return pd.json_normalize([event for event in self.events if event['player']['name'] == player_name])

    def xgoals_by_team(self):
        all_shots = pd.concat((self.shots, self.get_events_by_type('penalty'))).sort_values(['matchPeriod', 'minute', 'second'])
        return tuple(np.sum(all_shots['shot.xg'][all_shots['team.name'] == team]) for team in self.teams)

    def calculate_match_stats(self):

        stats = {'Performance': {}, 'Attacking': {}, 'Defending': {}, 'Disciplinary': {}}
        teams = self.teams

        # performance

        possessions = {}
        for event in self.events:
            if 'possession' in event and event['possession'] is not None:
                possession_id = event['possession']['id']
                if possession_id not in possessions:
                    possessions[possession_id] = {'team': event['possession']['team']['name'],
                                                  'duration': event['possession']['duration']}
                else:
                    continue
        total_durations = [0, 0]
        for id in possessions:
            if possessions[id]['team'] == 'Denmark':
                total_durations[0] += float(possessions[id]['duration'])
            elif possessions[id]['team'] == 'Finland':
                total_durations[1] += float(possessions[id]['duration'])
        stats['Performance']['Possession (%)'] = [int(total_durations[0] / sum(total_durations)*100),
                                                  int(total_durations[1] / sum(total_durations)*100)]

        passes = self.passes
        pass_attempts = [(passes['team.name'] == team).sum() for team in teams]
        pass_completes = [((passes['team.name'] == team) & (passes['pass.accurate'])).sum() for team in teams]
        stats['Performance']['Passing accuracy (%)'] = [int(pass_completes[0] / pass_attempts[0] * 100),
                                                        int(pass_completes[1] / pass_attempts[1] * 100)]
        stats['Performance']['Passes attempted'] = pass_attempts
        stats['Performance']['Passes completed'] = pass_completes

        # attacking
        shots = self.shots
        penalty = self.get_events_by_type('penalty')
        interceptions = self.get_events_by_type('interception')
        stats['Attacking']['Goals'] = [((shots['team.name'] == team) & (shots['shot.isGoal'])).sum() + ((penalty['team.name'] == team) & (penalty['shot.isGoal'])).sum() for team in teams]
        stats['Attacking']['Total attempts'] = [(shots['team.name'] == team).sum() + (penalty['team.name'] == team).sum() for team in teams]
        stats['Attacking']['On target'] = [((shots['team.name'] == team) & (shots['shot.onTarget'])).sum() + ((penalty['team.name'] == team) & (penalty['shot.onTarget'])).sum() for team in teams]
        stats['Attacking']['Off target'] = [((shots['team.name'] == team) & (~shots['shot.onTarget'])).sum() + ((penalty['team.name'] == team) & (~penalty['shot.onTarget'])).sum() for team in teams]

        blocks = [((interceptions['team.name'] == team) & (interceptions['type.secondary'].astype(str).str.contains('shot_block'))).sum() for team in teams]
        stats['Attacking']['Blocked'] = [blocks[1], blocks[0]]
        stats['Attacking']['Total attempts'][0] += stats['Attacking']['Blocked'][0]
        stats['Attacking']['Total attempts'][1] += stats['Attacking']['Blocked'][1]

        stats['Attacking']['Woodwork'] = [0, 0]
        stats['Attacking']['Corners taken'] = [(self.get_events_by_type('corner')['team.name'] == team).sum() for team in teams]
        stats['Attacking']['Offsides'] = [(self.get_events_by_type('offside')['team.name'] == team).sum() for team in teams]

        # defending
        duels = self.get_events_by_type('duel')
        stats['Defending']['Balls recovered'] = [((duels['team.name'] == team) & (duels['type.secondary'].astype(str).str.contains('recovery'))).sum() for team in teams]
        stats['Defending']['Tackles'] = [((duels['team.name'] == team) & (duels['type.secondary'].astype(str).str.contains('sliding_tackle'))).sum() for team in teams]
        stats['Defending']['Blocks'] = blocks
        stats['Defending']['Clearances completed'] = [(self.get_events_by_type('clearance')['team.name'] == team).sum() for team in teams]

        # disciplinary
        infractions = self.get_events_by_type('infraction')
        stats['Disciplinary']['Yellow cards'] = [((infractions['team.name'] == team) & (infractions['type.secondary'].astype(str).str.contains('yellow_card'))).sum() for team in teams]
        stats['Disciplinary']['Red cards'] = [((infractions['team.name'] == team) & (infractions['type.secondary'].astype(str).str.contains('red_card'))).sum() for team in teams]
        stats['Disciplinary']['Fouls committed'] = [(infractions['team.name'] == team).sum() for team in teams]

        print(stats)
        return stats


class Wyscout_all:
    provider = 'wyscout'
    pitch_dims = (100, 100)

    def __init__(self, path, match_id):
        self.path = path
        self.match_id = match_id
        self.events = self.load_events()
        self.event_types = pd.unique([event['type']['primary'] for event in self.events])
        self.shots = self.get_event_df('shot')
        self.passes = self.get_event_df('pass')

    def load_events(self):
        file = self.path + str(self.match_id) + "_events.json"
        if not os.path.isfile(file):
            with ZipFile(self.path + str(self.match_id) + ".zip", 'r') as f:
                f.extractall(self.path)
        return json.load(open(file))['events']

    def get_event_df(self, event_type: str):
        return pd.json_normalize([event for event in self.events if event['type']['primary'] == event_type])
