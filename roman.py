from statsbomb import Statsbomb
from wyscout import Wyscout
#import plotting as plt #naming-conflict
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

INFO = {
    'statsbomb': {
        'path': "data/statsbomb360/",
        'match_id': 3788742
    },
    'wyscout': {
        'path': "data/wyscout/",
        'match_id': 5034297
    },
    'tracab': {
        'path': "data/tracab/",
        'match_id': None
    },
    'team_names': ['Denmark', 'Finland'],
    'competition_stage': 'Group Stage'
}

wyscout = Wyscout(INFO['wyscout']['path'],
                  INFO['wyscout']['match_id'],
                  INFO['team_names'],
                  INFO['competition_stage'])

###############################################################
###############################################################
###############################################################
#FUNCTIONS IMPLEMENTED HERE

#shows a map of passes to 'playername' in red (origin at red dot)
#and passes played by 'playername' in blue (origin at blue dot)
def passing_pattern(playername):
    pcdf = wyscout.get_events_by_type('pass')
    title = ['StartX', 'StartY', 'EndX', 'EndY', 'length']
    coordslistE = []
    coordslistE.append(title)
    coordslistJ = []
    coordslistJ.append(title)

    # Passes recieved
    for ind in pcdf.index:
        if pcdf['pass.recipient.name'][ind] == playername:
            x = pcdf['location.x'][ind]
            y = pcdf['location.y'][ind]
            recX = pcdf['pass.endLocation.x'][ind]
            recY = pcdf['pass.endLocation.y'][ind]
            length = pcdf['pass.length'][ind]
            coords = [x, y, recX, recY, length]
            coordslistE.append(coords)

    # Passes played
    for ind in pcdf.index:
        if pcdf['player.name'][ind] == playername:
            x = pcdf['location.x'][ind]
            y = pcdf['location.y'][ind]
            recX = pcdf['pass.endLocation.x'][ind]
            recY = pcdf['pass.endLocation.y'][ind]
            length = pcdf['pass.length'][ind]
            coords = [x, y, recX, recY, length]
            coordslistJ.append(coords)

    # turn lists into dataframes
    #This step could be skipped for sure, but too lazy rn
    coordsdfE = pd.DataFrame(coordslistE[1:], columns=coordslistE[0])
    coordsdfJ = pd.DataFrame(coordslistJ[1:], columns=coordslistJ[0])

    # turn dataframes back into lists for plotting
    # names "Jx" and "Ex" and such are non-sensical, were kept this way from earlier iteration
    listEx = []
    listEy = []
    listJx = []
    listJy = []
    for ind in coordsdfE.index:
        listEx.append(coordsdfE['StartX'][ind])
        listEx.append(coordsdfE['EndX'][ind])
        listEy.append(coordsdfE['StartY'][ind])
        listEy.append(coordsdfE['EndY'][ind])
    for ind in coordsdfJ.index:
        listJx.append(coordsdfJ['StartX'][ind])
        listJx.append(coordsdfJ['EndX'][ind])
        listJy.append(coordsdfJ['StartY'][ind])
        listJy.append(coordsdfJ['EndY'][ind])

    print("Passes recieved: " + str(len(listEx) /2))
    print("Passes played: " + str(len(listJx) /2))
    # creates passing pattern map
    for i in range(0, len(listEx), 2):
        plt.plot(listEx[i:i + 2], listEy[i:i + 2], 'red')  # draw recieving passes
        plt.plot(listJx[i:i + 2], listJy[i:i + 2], 'blue')  # draw played passes
        plt.plot(listEx[::2], listEy[::2], 'ro')  # draw points to show where ball started for recieved passes
        plt.plot(listJx[::2], listJy[::2], 'bo')  # draw points to show where ball started for played passes
    plt.xlim(0, 100) #show "entire pitch"
    plt.ylim(0, 100)
    plt.title('Passing pattern of ' + str(playername))
    plt.show()

#shows a map of all actions by 'playername'
#needs color_coder(playername, ind) to function
def action_map(player1):
    playerframe1 = pd.json_normalize([event for event in wyscout.events if event['player']['name'] == player1])
    #primary = set()

    #print(playerframe.to_string())

    for ind in playerframe1.index:
        plt.scatter(playerframe1['location.x'][ind], playerframe1['location.y'][ind], c = color_coder(player1,ind) )
        #primary.add(playerframe['type.primary'][ind])
    plt.xlim(0, 100)
    plt.ylim(0, 100)
    plt.title('All actions by ' + str(player1))
    plt.show()
    #print(primary)

#returns different colors for different primary event types
#only event types that were present in DEN-FIN are accounted for
#this is pretty much hard-coding, but it allows for personal preference
#by making "good actions" green and "bad actions" red for example
#defensive = red-ish
#attacking/build-up = blue-ish
#other = green
def color_coder(playername,ind):
    playerframe = pd.json_normalize([event for event in wyscout.events if event['player']['name'] == playername])
    if playerframe['type.primary'][ind] == 'pass':
        return '#FF000000'
    if playerframe['type.primary'][ind] == 'duel':
        print('duel')
        return 'red'
    if playerframe['type.primary'][ind] == 'game_interruption':
        return 'black'
    if playerframe['type.primary'][ind] == 'throw_in':
        print('throwin')
        return 'black'
    if playerframe['type.primary'][ind] == 'interception':
        print('interception')
        return 'darkred'
    if playerframe['type.primary'][ind] == 'touch':
        print('touch')
        return 'green'
    if playerframe['type.primary'][ind] == 'infraction':
        print('infraction')
        return 'hotpink'
    if playerframe['type.primary'][ind] == 'free_kick':
        print('free kick')
        return 'purple'
    if playerframe['type.primary'][ind] == 'corner':
        print('corner')
        return 'blue'
    if playerframe['type.primary'][ind] == 'shot':
        print('shot')
        return 'deepskyblue'
    if playerframe['type.primary'][ind] == 'shot_against':
        return 'black'
    if playerframe['type.primary'][ind] == 'clearance':
        return 'black'
    if playerframe['type.primary'][ind] == 'goal_kick':
        return 'black'
    if playerframe['type.primary'][ind] == 'goalkeeper_exit':
        return 'black'
    if playerframe['type.primary'][ind] == 'acceleration':
        return 'aqua'
    if playerframe['type.primary'][ind] == 'fairplay':
        return 'yellowgreen'
    if playerframe['type.primary'][ind] == 'offside':
        print('offside')
        return 'crimson'
    if playerframe['type.primary'][ind] == 'penalty':
        return 'black'
    else:
        return 'black'

def compare_player_actions(player1, player2):

    playerframe1 = pd.json_normalize([event for event in wyscout.events if event['player']['name'] == player1])
    for ind in playerframe1.index:
        plt.scatter(playerframe1['location.x'][ind], playerframe1['location.y'][ind], c = 'blue' )

    playerframe2 = pd.json_normalize([event for event in wyscout.events if event['player']['name'] == player2])
    for ind in playerframe2.index:
        plt.scatter(playerframe2['location.x'][ind], playerframe2['location.y'][ind], c = 'red' )

    plt.xlim(0, 100)
    plt.ylim(0, 100)
    plt.title(str(player1) + ': blue, ' + str(player2) + ': red')
    plt.show()

def compare_players(*players):
    color = ['blue', 'red', 'green', 'yellow', 'orange', 'black', 'white', 'purple']
    i = 0
    for player in players:
        playerframe = pd.json_normalize([event for event in wyscout.events if event['player']['name'] == player])
        for ind in playerframe.index:
            plt.scatter(playerframe['location.x'][ind], playerframe['location.y'][ind], c = color[i] )
        i += 1
    plt.xlim(0, 100)
    plt.ylim(0, 100)
    plt.title('plot title')
    plt.show()
###############################################################
###############################################################
###############################################################
#OWN NOTES WITH RANDOM LINES OF CODE THAT I KEEP FORGETTING

#print the entire matchstats-dataframe (no ...)
#print(wyscout.playerstats().to_string())

#print the 1 1 element out of matchstats-dataframe
#print(wyscout.matchstats().iloc[1,1])

#print df with all passes
#print(wyscout.get_events_by_type('goalkeeper_exit').to_string())

#===================
#===================
#==================

#Selects only certain columns from a Dataframe
#cols = [3,4,23,24,30,31]
#passdf = wyscout.get_events_by_type('pass')[wyscout.get_events_by_type('pass').columns[cols]]
#print(passdf.to_string())


#creates map of all passes played by and to M. Braithwaite
#passing_pattern('M. Braithwaite')

#prints all players (makes calling functions easier lol)
#missing: Leo Väisänen, doesn't have any events in wyscout (was subbed on in 90th minute)
players = set()
for event in wyscout.events:
    players.add(event['player']['name'])
print(*players, sep='\n')

###############################################################
###############################################################
###############################################################
#CALL FUNCTIONS HERE

#compare_player_actions('M. Jensen', 'C. Eriksen')

#compare_player_actions('M. Jensen', 'C. Eriksen')

action_map('M. Jensen')






