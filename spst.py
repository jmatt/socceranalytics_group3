import numpy as np
import json
import pprint
import matplotlib.pyplot as plt
import pandas
import bz2

def spst(data):
    #getting the statsbomb events of the match
    zipfile = bz2.BZ2File('data/statsbomb360/events/3788742.json.bz2')
    data = zipfile.read()
    newfilepath  = ( 'data/statsbomb360/events/3788742.json' )
    open(newfilepath, 'wb').write(data)
    with open('data/statsbomb360/events/3788742.json', encoding='utf-8') as file:
        events=json.load(file)
    events = pandas.json_normalize(events)



    #get the potential setpieces
    passes_denmark_type = events[(events["type.name"]=="Pass") & (events["possession_team.name"]=="Denmark")]["pass.type.id"]
    passes_denmark_name = events[(events["type.name"]=="Pass") & (events["possession_team.name"]=="Denmark")]["player.name"]
    shots_denmark_type = events[(events["type.name"]=="Shot") & (events["possession_team.name"]=="Denmark")]["shot.type.id"]
    shots_denmark_name = events[(events["type.name"]=="Shot") & (events["possession_team.name"]=="Denmark")]["player.name"]
    passes_denmark_type = np.asarray(passes_denmark_type)
    passes_denmark_name = np.asarray(passes_denmark_name)
    shots_denmark_type = np.asarray(shots_denmark_type)
    shots_denmark_name = np.asarray(shots_denmark_name)

    passes_finland_type = events[(events["type.name"]=="Pass") & (events["possession_team.name"]=="Finland")]["pass.type.id"]
    passes_finland_name = events[(events["type.name"]=="Pass") & (events["possession_team.name"]=="Finland")]["player.name"]
    shots_finland_type = events[(events["type.name"]=="Shot") & (events["possession_team.name"]=="Finland")]["shot.type.id"]
    shots_finland_name = events[(events["type.name"]=="Shot") & (events["possession_team.name"]=="Finland")]["player.name"]
    passes_finland_type = np.asarray(passes_finland_type)
    passes_finland_name = np.asarray(passes_finland_name)
    shots_finland_type = np.asarray(shots_finland_type)
    shots_finland_name = np.asarray(shots_finland_name)



    #get a list of lists with the
    denmark_setpieces = [[], []]
    for i in range(0, len(passes_denmark_type)):
        if passes_denmark_type[i] == 61.0 or passes_denmark_type[i] == 62.0:
            denmark_setpieces[0].append(passes_denmark_type[i])
            denmark_setpieces[1].append(passes_denmark_name[i])
    for i in range(0, len(shots_denmark_type)):
        if shots_denmark_type[i] == 61.0 or shots_denmark_type[i] == 62.0 or shots_denmark_type[i] == 88.0:
            denmark_setpieces[0].append(shots_denmark_type[i])
            denmark_setpieces[1].append(shots_denmark_name[i])

    finland_setpieces = [[], []]
    for i in range(0, len(passes_finland_type)):
        if passes_finland_type[i] == 61.0 or passes_finland_type[i] == 62.0:
            finland_setpieces[0].append(passes_finland_type[i])
            finland_setpieces[1].append(passes_finland_name[i])
    for i in range(0, len(shots_finland_type)):
        if shots_finland_type[i] == 61.0 or shots_finland_type[i] == 62.0 or shots_finland_type[i] == 88.0:
            finland_setpieces[0].append(shots_finland_type[i])
            finland_setpieces[1].append(shots_finland_name[i])



    #create a list by set piece takers
    denmark_takers = [[], []]  # List with [Name, [number of corners, free kicks, pens]]
    for i in range(0, len(denmark_setpieces[0])):
        if denmark_setpieces[1][i] in denmark_takers[0]:
            y = denmark_takers[0].index(denmark_setpieces[1][i])
            if denmark_setpieces[0][i] == 61.0:
                denmark_takers[1][y][0] += 1
            if denmark_setpieces[0][i] == 62.0:
                denmark_takers[1][y][1] += 1
            if denmark_setpieces[0][i] == 88.0:
                denmark_takers[1][y][2] += 1
        else:
            denmark_takers[0].append(denmark_setpieces[1][i])
            denmark_takers[1].append([0, 0, 0])
            y = denmark_takers[0].index(denmark_setpieces[1][i])
            if denmark_setpieces[0][i] == 61.0:
                denmark_takers[1][y][0] += 1
            if denmark_setpieces[0][i] == 62.0:
                denmark_takers[1][y][1] += 1
            if denmark_setpieces[0][i] == 88.0:
                denmark_takers[1][y][2] += 1

    finland_takers = [[], []]  # Array with Name, [number of corners, free kicks, pens]
    for i in range(0, len(finland_setpieces[0])):
        if finland_setpieces[1][i] in finland_takers[0]:
            y = finland_takers[0].index(finland_setpieces[1][i])
            if finland_setpieces[0][i] == 61.0:
                finland_takers[1][y][0] += 1
            if finland_setpieces[0][i] == 62.0:
                finland_takers[1][y][1] += 1
            if finland_setpieces[0][i] == 88.0:
                finland_takers[1][y][2] += 1
        else:
            finland_takers[0].append(finland_setpieces[1][i])
            finland_takers[1].append([0, 0, 0])
            y = finland_takers[0].index(finland_setpieces[1][i])
            if finland_setpieces[0][i] == 61.0:
                finland_takers[1][y][0] += 1
            if finland_setpieces[0][i] == 62.0:
                finland_takers[1][y][1] += 1
            if finland_setpieces[0][i] == 88.0:
                finland_takers[1][y][2] += 1


    #Plot and save the Graphs
    labels = denmark_takers[0]
    corners = [item[0] for item in denmark_takers[1]]
    freekicks = [item[1] for item in denmark_takers[1]]
    pens = [item[2] for item in denmark_takers[1]]
    width = 0.35



    fig, ax  = plt.subplots()
    ax.bar(labels, corners, width,  label='Corners')
    ax.bar(labels, freekicks, width,  bottom=corners,label='Free Kicks')
    ax.bar(labels, pens, width,  bottom=freekicks,label='Penalties')
    ax.set_ylabel('Setpieces')
    ax.set_title('Setpieces by type and players for Denmark')
    ax.legend()
    #f = plt.figure()
    #f.set_figwidth(50)
    #f.set_figheight(20)
    plt.gcf().autofmt_xdate()
    plt.gcf().subplots_adjust(bottom=0.35)
    plt.savefig("spt_denmark.png")
    plt.show()

    labels = finland_takers[0]
    corners = [item[0] for item in finland_takers[1]]
    freekicks = [item[1] for item in finland_takers[1]]
    pens = [item[2] for item in finland_takers[1]]
    width = 0.35
    fig, bx  = plt.subplots()
    bx.bar(labels, corners, width,  label='Corners')
    bx.bar(labels, freekicks, width,  bottom=corners,label='Free Kicks')
    bx.bar(labels, pens, width,  bottom=freekicks,label='Penalties')
    bx.set_ylabel('Setpieces')
    bx.set_title('Setpieces by type and players for Finland')
    bx.legend()
    plt.gcf().autofmt_xdate()
    plt.gcf().subplots_adjust(bottom=0.35)
    plt.savefig("spt_finland.png")
    plt.show()
