from wyscout import Wyscout_all
import numpy as np

def relevantpasses(team_name, wyscout):
    # ---- find relevant possesion chain start and end points of relevant passes ------
    # relevant passes are completed passes of a possesion chains which ends in the last third
    possloc = []  # leere Liste
    for i in range(0, len(wyscout.passes)):
        if wyscout.passes.iloc[i, 26] == True:
            if wyscout.passes.iloc[i, 42] >= 0:
                if wyscout.passes.iloc[i, 18] == team_name:
                    possloc.append([wyscout.passes.iloc[i, 40], wyscout.passes.iloc[i, 41], wyscout.passes.iloc[i, 42],
                                    wyscout.passes.iloc[i, 43]])

    # ---- remove duplicates ------
    def_loc = []
    def_loc.append(possloc[0])  # erster Eintrag in Matrize
    for i in range(1, len(possloc)):
        if possloc[i] != possloc[i - 1]:
            def_loc.append(possloc[i])

    # ---- find start and end locations of passes of a possesion chain and save in list ------
    list_pass_chain = []
    for k in range(0, len(def_loc)):
        temp = []
        for i in range(0, len(wyscout.passes)):
            if [wyscout.passes.iloc[i, 40], wyscout.passes.iloc[i, 41], wyscout.passes.iloc[i, 42],
                wyscout.passes.iloc[i, 43]] == def_loc[k]:
                temp.append([wyscout.passes.iloc[i, 15], wyscout.passes.iloc[i, 16], wyscout.passes.iloc[i, 33],
                             wyscout.passes.iloc[i, 34]])
        list_pass_chain.append(temp)

    # ---- create pass chain ------
    pass_chain = []
    for i in range(0, len(list_pass_chain)):
        temp = []
        if list_pass_chain[i][0][0] == def_loc[i][0] and list_pass_chain[i][0][1] == def_loc[i][1]:
            temp.append([def_loc[i][0], def_loc[i][1]])
            temp = whatafunction(list_pass_chain, temp, i, def_loc)
        else:
            temp.append([def_loc[i][0], def_loc[i][1]])
            temp.append([list_pass_chain[i][0][0], list_pass_chain[i][0][1]])
            temp = whatafunction(list_pass_chain, temp, i, def_loc)
        pass_chain.append(temp)

    # ---- remove every chain smaller than 4 ------
    def_pass_chain = []
    for i in range(0, len(pass_chain)):
        if len(pass_chain[i]) >= 4:
            def_pass_chain.append(pass_chain[i])
    # print(def_pass_chain)

    return pass_chain


def findzones(goalloc, u, my_array, sum):
    ran = range(0, 10)
    for i in ran:
        for j in ran:
            count = 0
            for k in range(0, len(goalloc)):
                if goalloc[k][1] != 0:
                    if int((goalloc[k][0] - 50) / 5) == j and int(
                            goalloc[k][1] / 10) == i:  # add the value if coordinate match
                        count += u
                        sum += u
            my_array[i, j] += count  # add the value to array
    return my_array, sum


def whatafunction(list_pass_chain, temp, i, def_loc):
    for h in range(len(list_pass_chain[i]) - 1):
        if list_pass_chain[i][h][2] == list_pass_chain[i][h + 1][0] and list_pass_chain[i][h][3] == list_pass_chain[i][h + 1][1]:
            if [list_pass_chain[i][h][2], list_pass_chain[i][h][3]]!=[0,0]:
                temp.append([list_pass_chain[i][h][2], list_pass_chain[i][h][3]])
        else:
            if [list_pass_chain[i][h][2], list_pass_chain[i][h][3]]!=[0,0]:
                temp.append([list_pass_chain[i][h][2], list_pass_chain[i][h][3]])
            if [list_pass_chain[i][h + 1][0], list_pass_chain[i][h + 1][1]]!=[0,0]:
                temp.append([list_pass_chain[i][h + 1][0], list_pass_chain[i][h + 1][1]])
    if list_pass_chain[i][-1][2] == def_loc[i][2] and list_pass_chain[i][-1][3] == def_loc[i][3]:
        if [def_loc[i][2], def_loc[i][3]]!=[0,0]:
            temp.append([def_loc[i][2], def_loc[i][3]])
    else:
        if [list_pass_chain[i][-1][2], list_pass_chain[i][-1][3]]!=[0,0]:
            temp.append([list_pass_chain[i][-1][2], list_pass_chain[i][-1][3]])
        if [def_loc[i][2], def_loc[i][3]]!=[0,0]:
            temp.append([def_loc[i][2], def_loc[i][3]])
    return temp


def goalsnassists(match_ids):
    # finding locations where goals were scored, only from shots (own Goals and penalties have an ununified structure -> pain)
    # further finding pass locations up to 2 passes before goal (problem: if goal is scored in a possesion chain whitout a pass
    # in it (i.e directly from corner or an interception with dribble) this chain won't be detected)
    goalloc = []
    pass1 = []
    pass2 = []
    pass3 = []
    for w in range(0, len(match_ids)):
        wyscout_all = Wyscout_all("data/wyscout/", match_ids[w])
        temp = []
        status = False
        for j in range(0, len(wyscout_all.shots)):
            if wyscout_all.shots.iloc[j, 27]:
                status = True
                goalloc.append([wyscout_all.shots.iloc[j, 15], wyscout_all.shots.iloc[j, 16]])
                if wyscout_all.shots.iloc[j, 43] >= 100:
                    temp.append(
                        [wyscout_all.shots.iloc[j, 39], wyscout_all.shots.iloc[j, 40], wyscout_all.shots.iloc[j, 41],
                         wyscout_all.shots.iloc[j, 42]])
                else:
                    temp.append(
                        [wyscout_all.shots.iloc[j, 40], wyscout_all.shots.iloc[j, 41], wyscout_all.shots.iloc[j, 42],
                         wyscout_all.shots.iloc[j, 43]])
        if status:
            list_pass_chain = []
            status3 = False
            for i in range(0, len(temp)):
                temp2 = []
                status2 = False
                for j in range(0, len(wyscout_all.passes)):
                    if [wyscout_all.passes.iloc[j, 40], wyscout_all.passes.iloc[j, 41], wyscout_all.passes.iloc[j, 42],
                        wyscout_all.passes.iloc[j, 43]] == temp[i]:
                        temp2.append(
                            [wyscout_all.passes.iloc[j, 15], wyscout_all.passes.iloc[j, 16],
                             wyscout_all.passes.iloc[j, 33],
                             wyscout_all.passes.iloc[j, 34]])
                        status2 = True
                if status2:
                    list_pass_chain.append(temp2)
                    status3 = True
            if status3:
                pass_chain = []
                for i in range(0, len(list_pass_chain)):
                    temp2 = []
                    if list_pass_chain[i][0][0] == temp[i][0] and list_pass_chain[i][0][1] == temp[i][1]:
                        temp2.append([temp[i][0], temp[i][1]])
                        temp2 = whatafunction(list_pass_chain, temp2, i, temp)
                    else:
                        temp2.append([temp[i][0], temp[i][1]])
                        temp2.append([list_pass_chain[i][0][0], list_pass_chain[i][0][1]])
                        temp2 = whatafunction(list_pass_chain, temp2, i, temp)
                    pass_chain.append(temp2)
                for i in range(0, len(pass_chain)):
                    pass1.append(pass_chain[i][-2])
                    if len(pass_chain[i]) >= 3:
                        pass2.append(pass_chain[i][-3])
                        if len(pass_chain[i]) >= 4:
                            pass3.append(pass_chain[i][-4])
    return goalloc, pass1, pass2, pass3


def probab(goalloc, pass1, pass2, pass3):
    # creating 100 zones inside of the second half of the field
    # assign every goal shot and pass to a zone and count the importance:
    # goal shot = 4
    # pass -1 = 3
    # pass -2 = 2
    # pass -3 = 1

    # create an array of the right size with zero
    my_array = np.zeros((10, 10))
    sum = 0

    # loop in the array

    my_array, sum = findzones(goalloc, 4, my_array, sum)
    my_array, sum = findzones(pass1, 3, my_array, sum)
    my_array, sum = findzones(pass2, 2, my_array, sum)
    my_array, sum = findzones(pass3, 1, my_array, sum)
    my_array = my_array / sum
    return my_array


def probabDefense(loc, sum):
    # creating 100 zones inside of the first half of the field

    # create an array of the right size with zero
    my_array = np.zeros((5, 5))

    # loop in the array
    ran = range(5)
    for i in ran:
        for j in ran:
            count = 0
            for k in range(len(loc)):
                if loc[k][0] <= 49:
                    if int(loc[k][0] / 10) == j and int(
                            loc[k][1] / 20) == i:  # add the value if coordinate match
                        count += 1
            my_array[i, j] += count  # add the value to array
    my_array = my_array / sum
    return my_array


def xvalue(my_array, pass_chain):
    my_array_norm = my_array / np.max(my_array)
    xvalue = []
    for j in range(0, len(pass_chain)):
        a = int((pass_chain[j][-1][0] - 50) / 5)
        b = int((pass_chain[j][-1][1]) / 10)
        if a >= 0 and a <= 9 and b <= 9:
            xvalue.append(my_array_norm[b, a])
        else:
            xvalue.append(0.1)
    return xvalue


def defense(wyscout,team):
    loc=[]

    # interceptions
    interceptions= wyscout.get_event_df('interception')
    for i in range(len(interceptions)):
        if interceptions.iloc[i, 18] == team:
            loc.append([interceptions.iloc[i, 15], interceptions.iloc[i, 16]])

    #duels
    duel = wyscout.get_event_df('duel')
    for i in range(len(duel)):
        if duel.iloc[i,18] == team:
            for j in range(len(duel.iloc[i,14])):
                if duel.iloc[i,14][j] == 'defensive_duel':
                    status=False
                    for k in range(len(duel.iloc[i,14])):
                        if duel.iloc[i,14][k] == 'recovery':
                            loc.append([duel.iloc[i,15],duel.iloc[i,16]])
                            status=True
                    if status == False and duel.iloc[i,33]==True:
                        loc.append([duel.iloc[i,15],duel.iloc[i,16]])

    # clearances
    clear=wyscout.get_event_df('clearance')
    for i in range(len(clear)):
        if clear.iloc[i,18] == team:
            loc.append([clear.iloc[i,15],clear.iloc[i,16]])

    return loc


def baddefense(wyscout,team):
    loc=[]

    duel= wyscout.get_event_df('duel')
    for i in range(len(duel)):
        if duel.iloc[i,18] == team:
            for j in range(len(duel.iloc[i,14])):
                if duel.iloc[i,14][j] == 'defensive_duel':
                    status=False
                    for k in range(len(duel.iloc[i,14])):
                        if duel.iloc[i,14][k] == 'recovery':
                            status=True
                    if status == False and duel.iloc[i,33] == False:
                        loc.append([duel.iloc[i,15],duel.iloc[i,16]])
    if team == 'Denmark':
        loc.append([6,52])
    return loc


